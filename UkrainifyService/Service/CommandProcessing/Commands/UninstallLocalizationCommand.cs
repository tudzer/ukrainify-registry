﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing.Commands
{
  [CommandName("UninstallLocalization")]
  internal class UninstallLocalizationCommand : Command<UninstallLocalizationCommandArgs>
  {
    private readonly IInstallerFacade _installerFacade;

    public UninstallLocalizationCommand(IInstallerFacade installerFacade)
    {
      _installerFacade = installerFacade;
    }

    protected override async Task<object> Execute(UninstallLocalizationCommandArgs args)
    {
      var installer = string.IsNullOrEmpty(args.Path)
        ? await _installerFacade.GetInstallerAsync(args.GameId, args.LocalizationId, args.Platform)
        : await _installerFacade.GetInstallerAsync(args.GameId, args.LocalizationId, args.Path);

      try
      {
        installer.Uninstall();
        return true;
      }
      catch (Exception e)
      {
        Trace.WriteLine(e.ToString());
        return false;
      }
      finally
      {
        installer.LocalizationState.Clear();
      }
    }
  }
}

