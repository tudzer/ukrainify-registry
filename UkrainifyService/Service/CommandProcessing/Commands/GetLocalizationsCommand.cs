﻿using System.Linq;
using System.Threading.Tasks;
using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing.Commands
{
  [CommandName("GetLocalizations")]
  internal class GetLocalizationsCommand : Command<GetLocalizationsCommandArgs>
  {
    private readonly IGameRegistry _gameRegistry;
    private readonly ILocalizationRegistry _localizationRegistry;

    public GetLocalizationsCommand(IGameRegistry gameRegistry, ILocalizationRegistry localizationRegistry)
    {
      _gameRegistry = gameRegistry;
      _localizationRegistry = localizationRegistry;
    }

    protected override Task<object> Execute(GetLocalizationsCommandArgs args)
    {
      var localizations = _localizationRegistry.GetLocalizations(args.GameId);
      var result = localizations.Select(localization => new LocalizationInfo
      {
        LocalizationId = localization.LocalizationId,
        Platfroms = localization.SupportedPlatforms.Where(platform => _gameRegistry.IsInstallationFound(args.GameId, platform)),
      });

      return Task.FromResult<object>(result);
    }
  }
}

