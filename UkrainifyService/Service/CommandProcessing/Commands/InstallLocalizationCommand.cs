﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing.Commands
{
  [CommandName("InstallLocalization")]
  internal class InstallLocalizationCommand : Command<InstallLocalizationCommandArgs>
  {
    private readonly IInstallerFacade _installerFacade;

    public InstallLocalizationCommand(IInstallerFacade installerFacade)
    {
      _installerFacade = installerFacade;
    }

    protected override async Task<object> Execute(InstallLocalizationCommandArgs args)
    {
      var installer = string.IsNullOrEmpty(args.Path)
        ? await _installerFacade.GetInstallerAsync(args.GameId, args.LocalizationId, args.Platform)
        : await _installerFacade.GetInstallerAsync(args.GameId, args.LocalizationId, args.Path);

      try
      {
        Directory.CreateDirectory(installer.LocalizationState.Path);
        installer.Install();
        return true;
      }
      catch (Exception e)
      {
        installer.LocalizationState.Backup.Restore();
        installer.LocalizationState.Clear();
        Trace.WriteLine(e.ToString());
        return false;
      }
    }
  }
}

