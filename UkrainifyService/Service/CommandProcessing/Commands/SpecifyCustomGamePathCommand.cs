﻿using System.Runtime.InteropServices;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using Ookii.Dialogs.WinForms;
using Ukrainify.Resources;
using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing.Commands
{
  [CommandName("SpecifyCustomGamePath")]
  internal class SpecifyCustomGamePathCommand : Command<SpecifyCustomGamePathCommandArgs>
  {
    private readonly IGameRegistry _gameRegistry;
    private readonly ILocalizationRegistry _localizationRegistry;
    private readonly IInstallerRegistry _installerRegistry;

    [DllImport("user32.dll")]
    private static extern IntPtr GetForegroundWindow();

    public SpecifyCustomGamePathCommand(IGameRegistry gameRegistry, ILocalizationRegistry localizationRegistry, IInstallerRegistry installerRegistry)
    {
      _gameRegistry = gameRegistry;
      _localizationRegistry = localizationRegistry;
      _installerRegistry = installerRegistry;
    }

    private static Task<string> SpecifyDirectoryAsync()
    {
      var tcs = new TaskCompletionSource<string>();

      var thread = new Thread(() =>
      {
        using (var dialog = new VistaFolderBrowserDialog())
        {
          var windowHandle = GetForegroundWindow();
          var parentWindow = new Window(windowHandle);

          dialog.Description = Strings.SpecifyGamePath;
          dialog.UseDescriptionForTitle = true;
          var dialogResult = dialog.ShowDialog(parentWindow);

          if (dialogResult == DialogResult.OK && !string.IsNullOrWhiteSpace(dialog.SelectedPath))
          {
            tcs.SetResult(dialog.SelectedPath);
          }
          else
          {
            tcs.SetResult(null);
          }
        }
      });

      thread.SetApartmentState(ApartmentState.STA);
      thread.Start();

      return tcs.Task;
    }

    protected override async Task<object> Execute(SpecifyCustomGamePathCommandArgs args)
    {
      var selectedPath = await SpecifyDirectoryAsync();
      if (selectedPath == null) return null;
      
      var gameInstallation = _gameRegistry.GetInstallation(args.GameId, selectedPath);
      Trace.WriteLine($"GameID: {args.GameId}, LocalizationID: {args.LocalizationId}");
      var localization = _localizationRegistry.GetLocalization(args.GameId, args.LocalizationId);

      var installer = await _installerRegistry.GetInstallerAsync(gameInstallation, localization);
      return new
      {
        IsSupported = installer != null,
        IsGameUpdateDetected = installer?.IsLocalizationModified ?? false,
        IsInstalled = installer?.IsInstalled ?? false,
        Path = selectedPath,
      };
    }
  }

  internal class Window : IWin32Window
  {
    public IntPtr Handle { get; private set; }

    public Window(IntPtr handle)
    {
      Handle = handle;
    }
  }
}

