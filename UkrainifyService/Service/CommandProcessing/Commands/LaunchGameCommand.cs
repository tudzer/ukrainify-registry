﻿using System.Threading.Tasks;
using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing.Commands
{
  [CommandName("LaunchGame")]
  internal class LaunchGameCommand : Command<LaunchGameCommandArgs>
  {
    private readonly IGameRegistry _gameRegistry;

    public LaunchGameCommand(IGameRegistry gameRegistry)
    {
      _gameRegistry = gameRegistry;
    }

    protected override Task<object> Execute(LaunchGameCommandArgs args)
    {
      var gameInstallation = string.IsNullOrEmpty(args.Path)
        ? _gameRegistry.GetInstallation(args.GameId, args.Platform)
        : _gameRegistry.GetInstallation(args.GameId, args.Path);
      gameInstallation.Launch();

      return Task.FromResult((object)null);
    }
  }
}
