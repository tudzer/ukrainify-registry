﻿using System.Threading.Tasks;
using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing.Commands
{
  [CommandName("IsLocalizationInstalled")]
  internal class IsLocalizationInstalledCommand : Command<IsLocalizationInstalledCommandArgs>
  {
    private readonly IInstallerFacade _installerFacade;

    public IsLocalizationInstalledCommand(IInstallerFacade installerFacade)
    {
      _installerFacade = installerFacade;
    }

    protected override async Task<object> Execute(IsLocalizationInstalledCommandArgs args)
    {
      var installer = await _installerFacade.GetInstallerAsync(args.GameId, args.LocalizationId, args.Platform);

      return installer.IsInstalled;
    }
  }
}

