﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ukrainify.CommandProcessing
{
  internal class ConsoleCommandProcessor : ICommandProcessor
  {
    private const int ParentProcessId = -1;

    [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
    private static extern bool AttachConsole(int processId);

    private readonly ICommandExecutor _commandExecutor;

    private static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
    {
      ContractResolver = new CamelCasePropertyNamesContractResolver()
    };

    public ConsoleCommandProcessor(ICommandExecutor commandExecutor)
    {
      AttachConsole(ParentProcessId);
      _commandExecutor = commandExecutor;
    }

    public async Task StartListeningAsync()
    {
      while (true)
      {
        var command = Console.ReadLine();
        if (string.IsNullOrEmpty(command)) continue;

        Trace.WriteLine($"Received command: {command}");
        var definition = JsonConvert.DeserializeObject<CommandDefinition>(command, JsonSerializerSettings);
        var commandResult = await _commandExecutor.ExecuteAsync(definition);
        var serializedResult = JsonConvert.SerializeObject(commandResult, JsonSerializerSettings);
        Console.WriteLine(serializedResult);
        Trace.WriteLine($"Sent command result: {serializedResult}");
      }
    }
  }
}
