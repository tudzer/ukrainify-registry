﻿using System.Threading.Tasks;

namespace Ukrainify.CommandProcessing
{
  internal interface ICommandProcessor
  {
    Task StartListeningAsync();
  }
}