﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing
{
  internal sealed class LocalizationInfo
  {
    [JsonProperty("localizationId")]
    public string LocalizationId { get; set; }

    [JsonProperty("platforms")]
    public IEnumerable<GamePlatform> Platfroms { get; set; }
  }
}
