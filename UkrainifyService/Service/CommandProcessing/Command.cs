﻿using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Ukrainify.CommandProcessing
{
  internal abstract class Command<T> : ICommand
  {
    public Task<object> Run(JToken args)
    {
      return Execute(args == null ? default : args.ToObject<T>());
    }

    protected abstract Task<object> Execute(T args);
  }
}
