﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Ukrainify.CommandProcessing
{
  internal class CommandDefinition
  {
    [JsonProperty("id")]
    public int CommandId { get; set; }

    public string Command { get; set; }

    public JToken Args { get; set; }

    public CommandResult CreateSuccessResult(object result)
    {
      return new CommandResult
      {
        CommandId = CommandId,
        Success = true,
        Result = result
      };
    }

    public CommandResult CreateFailureResult()
    {
      return new CommandResult
      {
        CommandId = CommandId,
        Success = false,
      };
    }
  }
}
