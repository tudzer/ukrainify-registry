﻿using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Ukrainify.CommandProcessing
{
  public interface ICommand
  {
    Task<object> Run(JToken args);
  }
}