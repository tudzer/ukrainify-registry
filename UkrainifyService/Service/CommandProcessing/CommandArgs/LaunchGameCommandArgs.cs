﻿using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing
{
  internal class LaunchGameCommandArgs
  {
    public string GameId { get; set; }

    public GamePlatform Platform { get; set; }

    public string Path { get; set; }
  }
}
