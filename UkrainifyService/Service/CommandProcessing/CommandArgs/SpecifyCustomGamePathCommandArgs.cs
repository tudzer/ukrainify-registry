﻿namespace Ukrainify.CommandProcessing
{
  internal class SpecifyCustomGamePathCommandArgs
  {
    public string GameId { get; set; }

    public string LocalizationId { get; set; }
  }
}
