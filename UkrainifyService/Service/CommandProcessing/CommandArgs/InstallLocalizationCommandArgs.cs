﻿using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing
{
  internal class InstallLocalizationCommandArgs
  {
    public string GameId { get; set; }

    public string LocalizationId { get; set; }

    public GamePlatform Platform { get; set; }

    public string Path { get; set; }
  }
}
