﻿using Ukrainify.L10n;

namespace Ukrainify.CommandProcessing
{
  internal class IsLocalizationInstalledCommandArgs
  {
    public string GameId { get; set; }

    public string LocalizationId { get; set; }

    public GamePlatform Platform { get; set; }
  }
}
