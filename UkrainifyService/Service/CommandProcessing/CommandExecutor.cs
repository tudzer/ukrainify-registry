﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Ukrainify.CommandProcessing
{
  internal class CommandExecutor : ICommandExecutor
  {
    private static IDictionary<string, ICommand> Commands { get; } = new Dictionary<string, ICommand>();

    public CommandExecutor(IComponentContext container)
    {
      var classTypes = Assembly.GetExecutingAssembly().GetTypes().Where(type => type.IsClass);

      foreach (var classType in classTypes)
      {
        var commandAttribute = classType.GetCustomAttribute<CommandName>();
        if (commandAttribute != null)
        {
          Commands[commandAttribute.Value] = (ICommand)container.Resolve(classType);
        }
      }
    }

    public async Task<CommandResult> ExecuteAsync(CommandDefinition definition)
    {
      if (!Commands.ContainsKey(definition.Command))
      {
        throw new Exception($"Unknown command '{definition.Command}'.");
      }

      try
      {
        var result = await Commands[definition.Command].Run(definition.Args);
        return definition.CreateSuccessResult(result);
      }
      catch (Exception ex)
      {
        Trace.WriteLine($"Failed to execute command '{definition.Command}':\n{ex}");
        return definition.CreateFailureResult();
      }
    }
  }
}
