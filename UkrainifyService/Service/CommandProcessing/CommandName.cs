﻿using System;

namespace Ukrainify.CommandProcessing
{
  [AttributeUsage(AttributeTargets.Class)]
  internal class CommandName : Attribute
  {
    public CommandName(string value)
    {
      Value = value;
    }

    public string Value { get; private set; }
  }
}
