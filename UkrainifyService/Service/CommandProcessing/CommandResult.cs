﻿using Newtonsoft.Json;

namespace Ukrainify.CommandProcessing
{
  internal class CommandResult
  {
    [JsonProperty("id")]
    public int CommandId { get; set; }

    public bool Success { get; set; }

    public object Result { get; set; }
  }
}
