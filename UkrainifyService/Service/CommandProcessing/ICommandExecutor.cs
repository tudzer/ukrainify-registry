﻿using Autofac;
using System.Threading.Tasks;

namespace Ukrainify.CommandProcessing
{
  internal interface ICommandExecutor
  {
    Task<CommandResult> ExecuteAsync(CommandDefinition definition);
  }
}