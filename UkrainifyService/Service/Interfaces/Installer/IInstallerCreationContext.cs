﻿namespace Ukrainify.L10n
{
  internal interface IInstallerCreationContext
  {
    string GameInstallationPath { get; }
  }
}
