﻿using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal interface IInstallerFacade
  {
    Task<IPackageInstaller> GetInstallerAsync(string gameId, string localizationId, GamePlatform platform);

    Task<IPackageInstaller> GetInstallerAsync(string gameId, string localizationId, string gameInstallationPath);
  }
}