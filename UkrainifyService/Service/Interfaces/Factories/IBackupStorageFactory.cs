﻿using Ukrainify.L10n.Integrity;

namespace Ukrainify.L10n
{
  internal interface IBackupStorageFactory
  {
    IBackupStorage Create(string gameInstallationPath, string basePath);
  }
}
