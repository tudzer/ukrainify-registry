﻿namespace Ukrainify.L10n
{
  internal interface IPackageInstallerFactory
  {
    IPackageInstaller Create(string gameInstallationPath, ILocalizationPackage package);
  }

  internal interface IPackageInstallerFactory<out TInstaller, in TContext>
    where TInstaller : IPackageInstaller
    where TContext : IPackageInstallerContext
  {
    TInstaller Create(TContext context, string packagePath, ILocalizationState localizationState);
  }
}
