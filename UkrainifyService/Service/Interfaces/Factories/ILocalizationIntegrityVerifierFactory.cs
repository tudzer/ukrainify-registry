﻿namespace Ukrainify.L10n.Integrity
{
  internal interface ILocalizationIntegrityVerifierFactory
  {
    ILocalizationIntegrityVerifier Create(string gameInstallationPath, ILocalizedFileRegistry localizedFileRegistry);
  }
}
