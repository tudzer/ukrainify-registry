﻿namespace Ukrainify.L10n
{
  internal interface IPackageInstallerContextFactory
  {
    IPackageInstallerContext Create(string gameInstallationPath, ILocalizationPackage package);
  }
}
