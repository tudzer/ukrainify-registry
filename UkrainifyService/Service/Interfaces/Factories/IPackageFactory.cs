﻿namespace Ukrainify.L10n
{
  internal interface IPackageFactory
  {
    ILocalizationPackage Create(string filePath, PackageFormat format);
  }
}