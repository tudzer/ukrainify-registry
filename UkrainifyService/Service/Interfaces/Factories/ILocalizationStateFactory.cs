﻿namespace Ukrainify.L10n
{
  internal interface ILocalizationStateFactory
  {
    ILocalizationState Create(string gameInstallationPath);
  }
}
