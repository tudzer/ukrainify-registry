﻿namespace Ukrainify.L10n
{
  internal interface ILocalizedFileRegistryFactory
  {
    ILocalizedFileRegistry Create(string basePath);
  }
}
