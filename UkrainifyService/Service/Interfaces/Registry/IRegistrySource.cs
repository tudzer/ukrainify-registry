﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal interface IRegistrySource
  {
    Task<ICollection<GameEntry>> GetEntriesAsync();
  }
}
