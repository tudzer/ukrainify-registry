﻿using System.Collections.Generic;

namespace Ukrainify.L10n
{
  internal interface ILocalizationRegistry
  {
    IEnumerable<ILocalization> GetLocalizations(string gameId);

    ILocalization GetLocalization(string gameId, string localizationId);
  }
}