﻿using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal interface IInstallerRegistry
  {
    Task<IPackageInstaller> GetInstallerAsync(IGameInstallation gameInstallation, ILocalization localization);
  }
}