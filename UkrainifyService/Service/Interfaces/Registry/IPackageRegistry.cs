﻿using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal interface IPackageRegistry
  {
    Task<ILocalizationPackage> GetPackageAsync(ILocalization localization, GamePlatform platform);

    Task<ILocalizationPackage> GetPackageAsync(PackageInfo packageInfo);
  }
}