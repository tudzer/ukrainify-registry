﻿namespace Ukrainify.L10n
{
  internal interface IGameRegistry
  {
    bool IsInstallationFound(string gameId, GamePlatform platform);

    IGameInstallation GetInstallation(string gameId, GamePlatform platform);

    IGameInstallation GetInstallation(string gameId, string path);
  }
}