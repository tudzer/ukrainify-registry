﻿using System;

namespace Ukrainify.L10n
{
  internal interface IGameInstallation : IEquatable<IGameInstallation>
  {
    string BasePath { get; }

    GamePlatform Platform { get; }

    void Launch();

    void Launch(string executablePath);
  }
}