﻿using System.Collections.Generic;

namespace Ukrainify.L10n
{
  internal interface ILocalization
  {
    string GameId { get; }

    string LocalizationId { get; }

    ICollection<GamePlatform> SupportedPlatforms { get; }

    IReadOnlyDictionary<GamePlatform, PackageInfo> Packages { get; }
  }
}