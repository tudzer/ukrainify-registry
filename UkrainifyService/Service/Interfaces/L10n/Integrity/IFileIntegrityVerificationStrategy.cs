﻿namespace Ukrainify.L10n.Integrity
{
  public interface IFileIntegrityVerificationStrategy
  {
    bool Execute(IFileDigest digest, string filePath);
  }
}