﻿namespace Ukrainify.L10n.Integrity
{
  public interface IFileDigestProvider<out T>
  {
    T GetDigest(string filePath);
  }
}
