﻿namespace Ukrainify.L10n
{
  internal interface IPackageLoadingStrategy
  {
    byte[] Execute(string filePath);
  }
}
