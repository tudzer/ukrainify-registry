﻿using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal interface IPackageDownloader
  {
    Task<string> RetrievePackageAsync(PackageInfo info);
  }
}