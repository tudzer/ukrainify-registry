﻿namespace Ukrainify.L10n
{
  internal interface ILocalizationPackage
  {
    PackageFormat Format { get; }

    string FilePath { get; }
  }
}
