﻿namespace Ukrainify.L10n
{
  internal interface IPackageLoader
  {
    byte[] LoadPackage(ILocalizationPackage package);
  }
}
