﻿using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal interface IPackageDownloadingStrategy
  {
    Task<string> Execute(string uri, string extension);
  }
}
