﻿namespace Ukrainify.L10n
{
  internal interface IPackageCreationStrategy<out T> where T : IPackageInstaller
  {
    T Execute(byte[] package);
  }
}
