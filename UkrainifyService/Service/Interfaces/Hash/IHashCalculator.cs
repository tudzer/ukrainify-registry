﻿namespace Ukrainify
{
  public interface IHashCalculator
  {
    string Calculate(string input);

    string Calculate(byte[] inputBytes);
  }
}
