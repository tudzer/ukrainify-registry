﻿using System;
using Ukrainify.PackageInstallers;

namespace Ukrainify.L10n
{
  internal class PackageInstallerContextFactory : IPackageInstallerContextFactory
  {
    public IPackageInstallerContext Create(string gameInstallationPath, ILocalizationPackage package)
    {
      switch (package.Format)
      {
        case PackageFormat.Proprietary:
        case PackageFormat.Archive:
          return new ArchivePackageInstallerContext(gameInstallationPath, package.FilePath);
        case PackageFormat.Custom:
          return new CustomPackageInstallerContext(gameInstallationPath);
        default:
          throw new NotImplementedException($"Unknown package format: {package.Format}");
      }
    }
  }
}
