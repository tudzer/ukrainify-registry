﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Ukrainify.PackageInstallers;

namespace Ukrainify.L10n
{
  internal class CustomPackageInstallerFactory: IPackageInstallerFactory<IPackageInstaller, CustomPackageInstallerContext>,
    IPackageInstallerFactory<IPackageInstaller, IPackageInstallerContext>
  {
    private readonly Dictionary<string, Assembly> _assemblyCache = new Dictionary<string, Assembly>();

    public IPackageInstaller Create(CustomPackageInstallerContext context, string assemblyPath, ILocalizationState localizationState)
    {
      if (!_assemblyCache.ContainsKey(assemblyPath))
      {
        var loadedAssembly = Assembly.LoadFile(assemblyPath);
        _assemblyCache.Add(assemblyPath, loadedAssembly);
      }

      var assembly = _assemblyCache[assemblyPath];
      return ReflectionUtils.CreateInstance<PackageInstaller<CustomPackageInstallerContext>>(assembly, context, localizationState);
    }

    public IPackageInstaller Create(IPackageInstallerContext context, string packagePath, ILocalizationState localizationState)
    {
      if (context is CustomPackageInstallerContext customPackageInstallerContext)
      {
        return Create(customPackageInstallerContext, packagePath, localizationState);
      }

      throw new ArgumentException("Unsupported context type.", nameof(context));
    }
  }
}
