﻿using System;
using Ukrainify.PackageInstallers;

namespace Ukrainify.L10n
{
  internal class ProprietaryPackageInstallerFactory : IPackageInstallerFactory<ProprietaryPackageInstaller, ArchivePackageInstallerContext>,
    IPackageInstallerFactory<IPackageInstaller, IPackageInstallerContext>
  {
    public ProprietaryPackageInstaller Create(ArchivePackageInstallerContext context, string packagePath, ILocalizationState localizationState)
    {
      return new ProprietaryPackageInstaller(context, localizationState);
    }

    public IPackageInstaller Create(IPackageInstallerContext context, string packagePath, ILocalizationState localizationState)
    {
      if (context is ArchivePackageInstallerContext proprietaryPackageInstallerContext)
      {
        return Create(proprietaryPackageInstallerContext, packagePath, localizationState);
      }

      throw new ArgumentException("Unsupported context type.", nameof(context));
    }
  }
}
