﻿using System;
using Ukrainify.PackageInstallers;

namespace Ukrainify.L10n
{
  internal class ArchivePackageInstallerFactory : IPackageInstallerFactory<ArchivePackageInstaller, ArchivePackageInstallerContext>,
    IPackageInstallerFactory<IPackageInstaller, IPackageInstallerContext>
  {
    public ArchivePackageInstaller Create(ArchivePackageInstallerContext context, string archivePath, ILocalizationState localizationState)
    {
      return new ArchivePackageInstaller(context, localizationState);
    }

    public IPackageInstaller Create(IPackageInstallerContext context, string packagePath, ILocalizationState localizationState)
    {
      if (context is ArchivePackageInstallerContext archivePackageInstallerContext)
      {
        return Create(archivePackageInstallerContext, packagePath, localizationState);
      }

      throw new ArgumentException("Unsupported context type.", nameof(context));
    }
  }
}
