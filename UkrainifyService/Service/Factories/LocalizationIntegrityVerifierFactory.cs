﻿using Ukrainify.L10n.Integrity;

namespace Ukrainify.L10n
{
  internal class LocalizationIntegrityVerifierFactory : ILocalizationIntegrityVerifierFactory
  {
    private readonly IFileDigestProvider<IFileDigest> _digestProvider;
    private readonly IFileIntegrityVerificationStrategy _integrityVerificationStrategy;

    public LocalizationIntegrityVerifierFactory(
      IFileDigestProvider<IFileDigest> digestProvider,
      IFileIntegrityVerificationStrategy integrityVerificationStrategy)
    {
      _digestProvider = digestProvider;
      _integrityVerificationStrategy = integrityVerificationStrategy;
    }

    public ILocalizationIntegrityVerifier Create(string gameInstallationPath, ILocalizedFileRegistry localizedFileRegistry)
    {
      return new LocalizationIntegrityVerifier(gameInstallationPath, localizedFileRegistry, _digestProvider, _integrityVerificationStrategy);
    }
  }
}
