﻿namespace Ukrainify.L10n
{
  internal class PackageFactory : IPackageFactory
  {
    public ILocalizationPackage Create(string filePath, PackageFormat format)
    {
      return new LocalizationPackage(filePath, format);
    }
  }
}
