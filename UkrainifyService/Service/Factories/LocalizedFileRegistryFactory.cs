﻿namespace Ukrainify.L10n
{
  internal class LocalizedFileRegistryFactory : ILocalizedFileRegistryFactory
  {
    public ILocalizedFileRegistry Create(string basePath)
    {
      return new LocalizedFileRegistry(basePath);
    }
  }
}
