﻿using System.IO;
using Ukrainify.L10n.Integrity;

namespace Ukrainify.L10n
{
  internal class LocalizationStateFactory : ILocalizationStateFactory
  {
    private readonly ILocalizedFileRegistryFactory _localizedFileRegistryFactory;
    private readonly IBackupStorageFactory _backupStorageFactory;
    private readonly ILocalizationIntegrityVerifierFactory _integrityVerifierFactory;
    private readonly IHashCalculator _hashCalculator;

    public LocalizationStateFactory(
      ILocalizedFileRegistryFactory localizedFileRegistryFactory,
      IBackupStorageFactory backupStorageFactory,
      ILocalizationIntegrityVerifierFactory integrityVerifierFactory,
      IHashCalculator hashCalculator
      )
    {
      _localizedFileRegistryFactory = localizedFileRegistryFactory;
      _backupStorageFactory = backupStorageFactory;
      _integrityVerifierFactory = integrityVerifierFactory;
      _hashCalculator = hashCalculator;
    }

    public ILocalizationState Create(string gameInstallationPath)
    {
      var localizationStatePath = GetLocalizationStatePath(gameInstallationPath);
      var localizedFileRegistry = _localizedFileRegistryFactory.Create(localizationStatePath);
      var backupStorage = _backupStorageFactory.Create(localizationStatePath, gameInstallationPath);
      var integrityVerifier = _integrityVerifierFactory.Create(gameInstallationPath, localizedFileRegistry);

      return new LocalizationState(localizationStatePath, localizedFileRegistry, backupStorage, integrityVerifier);
    }

    private string GetLocalizationStatePath(string gameInstallationPath)
    {
      #if DEBUG
      var unresolvedPath = Properties.Settings.Default.DebugStateDirectoryPath;
      var stateDirectoryPath = System.Environment.ExpandEnvironmentVariables(unresolvedPath);
      #else
      var stateDirectoryPath = Properties.Settings.Default.StateDirectoryPath;
      #endif
      var gameDirectoryName = PathUtils.GetDeepestDirectoryName(gameInstallationPath);
      var gameInstallationPathHash = _hashCalculator.Calculate(gameInstallationPath);
      var uniqueGameDirectoryName = $"{gameDirectoryName}_{gameInstallationPathHash}";

      return Path.Combine(stateDirectoryPath, uniqueGameDirectoryName);
    }
  }
}
