﻿using Ukrainify.L10n.Integrity;

namespace Ukrainify.L10n
{
  internal class BackupStorageFactory : IBackupStorageFactory
  {
    public IBackupStorage Create(string basePath, string gameInstallationPath)
    {
      return new BackupStorage(basePath, gameInstallationPath);
    }
  }
}
