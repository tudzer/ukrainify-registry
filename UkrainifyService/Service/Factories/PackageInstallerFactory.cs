﻿using System.Collections.Generic;

namespace Ukrainify.L10n
{
  internal class PackageInstallerFactory : IPackageInstallerFactory
  {
    private readonly IPackageInstallerContextFactory _contextFactory;
    private readonly ILocalizationStateFactory _localizationStateFactory;
    private readonly Dictionary<PackageFormat, IPackageInstallerFactory<IPackageInstaller, IPackageInstallerContext>> _installerFactories;

    public PackageInstallerFactory(
      IPackageInstallerContextFactory contextFactory,
      ILocalizationStateFactory localizationStateFactory,
      ProprietaryPackageInstallerFactory proprietaryInstallerFactory,
      CustomPackageInstallerFactory customInstallerFactory,
      ArchivePackageInstallerFactory archiveInstallerFactory
      )
    {
      _contextFactory = contextFactory;
      _localizationStateFactory = localizationStateFactory;
      _installerFactories = new Dictionary<PackageFormat, IPackageInstallerFactory<IPackageInstaller, IPackageInstallerContext>>
      {
        { PackageFormat.Proprietary, proprietaryInstallerFactory },
        { PackageFormat.Archive, archiveInstallerFactory },
        { PackageFormat.Custom, customInstallerFactory },
      };
    }

    public IPackageInstaller Create(string gameInstallationPath, ILocalizationPackage package)
    {
      var context = _contextFactory.Create(gameInstallationPath, package);
      var localizationState = _localizationStateFactory.Create(gameInstallationPath);
      var packageInstallerFactory = _installerFactories[package.Format];
      var packageInstaller = packageInstallerFactory.Create(context, package.FilePath, localizationState);

      return packageInstaller;
    }
  }
}
