﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal class PackageRegistry : IPackageRegistry
  {
    private readonly IPackageDownloader _packageLoader;
    private readonly IPackageFactory _packageFactory;
    private readonly IDictionary<PackageInfo, ILocalizationPackage> _loadedPackages = new Dictionary<PackageInfo, ILocalizationPackage>();
    private readonly IDictionary<Tuple<ILocalization, GamePlatform>, PackageInfo> _loadedPackageRefs = new Dictionary<Tuple<ILocalization, GamePlatform>, PackageInfo>();

    public PackageRegistry(IPackageDownloader packageLoader, IPackageFactory packageFactory)
    {
      _packageLoader = packageLoader;
      _packageFactory = packageFactory;
    }

    public async Task<ILocalizationPackage> GetPackageAsync(ILocalization localization, GamePlatform platform)
    {
      var key = Tuple.Create(localization, platform);
      if (_loadedPackageRefs.TryGetValue(key, out var loadedPackageInfo))
      {

        return _loadedPackages[loadedPackageInfo];
      }

      var packageInfo = localization.Packages[platform];
      var package = await LoadPackageAsync(packageInfo);

      _loadedPackages.Add(packageInfo, package);
      UpdateLoadedPackageRefs(packageInfo);

      return package;
    }

    public async Task<ILocalizationPackage> GetPackageAsync(PackageInfo packageInfo)
    {
      if (_loadedPackages.TryGetValue(packageInfo, out var existingPackage))
      {
        return existingPackage;
      }

      var package = await LoadPackageAsync(packageInfo);

      _loadedPackages.Add(packageInfo, package);
      UpdateLoadedPackageRefs(packageInfo);

      return package;
    }

    private async Task<ILocalizationPackage> LoadPackageAsync(PackageInfo packageInfo)
    {
      var filePath = await _packageLoader.RetrievePackageAsync(packageInfo);
      return _packageFactory.Create(filePath, packageInfo.Format);
    }

    private void UpdateLoadedPackageRefs(PackageInfo packageInfo)
    {
      foreach (var platform in packageInfo.Platforms)
      {
        var key = Tuple.Create(packageInfo.Localization, platform);
        _loadedPackageRefs.Add(key, packageInfo);
      }
    }
  }
}
