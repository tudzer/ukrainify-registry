﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal class LocalizationRegistry : ILocalizationRegistry
  {
    private readonly IRegistrySource _registrySource;
    private readonly IDictionary<string, List<ILocalization>> _localizations = new Dictionary<string, List<ILocalization>>();

    private LocalizationRegistry(IRegistrySource registrySource)
    {
      _registrySource = registrySource;
    }

    private void AddLocalization(string gameId, ILocalization localization)
    {
      if (_localizations.TryGetValue(gameId, out var localizations))
      {
        localizations.Add(localization);
      }
      else
      {
        _localizations.Add(gameId, new List<ILocalization> { localization });
      }
    }

    public IEnumerable<ILocalization> GetLocalizations(string gameId)
    {
      return _localizations.TryGetValue(gameId, out var localizations)
        ? localizations
        : new List<ILocalization>();
    }

    public ILocalization GetLocalization(string gameId, string localizationId)
    {
      return GetLocalizations(gameId).First(localization => localization.LocalizationId == localizationId);
    }


    private async Task InitializeAsync()
    {
      var entries = await _registrySource.GetEntriesAsync();
      foreach (var gameEntry in entries)
      {
        foreach (var localizationEntry in gameEntry.Localizations)
        {
          var manifest = new Localization(gameEntry.GameId, localizationEntry);
          AddLocalization(gameEntry.GameId, manifest);
        }
      }
    }

    public static async Task<ILocalizationRegistry> CreateAsync(IRegistrySource registrySource)
    {
      var registry = new LocalizationRegistry(registrySource);
      await registry.InitializeAsync();
      return registry;
    }
  }
}
