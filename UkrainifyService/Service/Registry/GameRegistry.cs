﻿using System.Linq;
using System.Collections.Generic;
using GameLib.Core;
using GameLib.Plugin.Steam;
using GameLib.Plugin.Gog;
using GameLib.Plugin.Epic;
using System.Threading.Tasks;
using System.IO;

namespace Ukrainify.L10n
{
  internal class GameRegistry : IGameRegistry
  {
    private readonly IRegistrySource _registrySource;
    private readonly IDictionary<string, IList<IGameInstallation>> _installations = new Dictionary<string, IList<IGameInstallation>>();
    private readonly IDictionary<string, IList<GameVersion>> _gameVersions = new Dictionary<string, IList<GameVersion>>();
    private readonly IDictionary<GamePlatform, IList<GameVersion>> _platformGames = new Dictionary<GamePlatform, IList<GameVersion>>();
    private readonly IDictionary<GamePlatform, ILauncher> _launchers = new Dictionary<GamePlatform, ILauncher>
    {
      { GamePlatform.Steam, new SteamLauncher() },
      { GamePlatform.Gog, new GogLauncher() },
      { GamePlatform.Epic, new EpicLauncher() },
    };

    private GameRegistry(IRegistrySource registrySource)
    {
      _registrySource = registrySource;
    }

    public bool IsInstallationFound(string gameId, GamePlatform platform)
    {
      return _installations[gameId].Any(installation => installation.Platform == platform);
    }

    public IGameInstallation GetInstallation(string gameId, GamePlatform platform)
    {
      return _installations[gameId].First(installation => installation.Platform == platform);
    }

    public IGameInstallation GetInstallation(string gameId, string path)
    {
      var gameVersion = _gameVersions[gameId].FirstOrDefault(version => File.Exists(Path.Combine(path, version.ExecutableRelativePath)));
      return new GameInstallation(path, gameVersion?.ExecutableRelativePath);
    }

    private async Task InitializeGamesAsync()
    {
      var entries = await _registrySource.GetEntriesAsync();
      foreach (var entry in entries)
      {
        foreach (var version in entry.Versions)
        {
          var game = new GameVersion
          {
            Id = entry.GameId,
            Title = version.GameTitle,
            ExecutableRelativePath = version.Exe,
          };
          
          if (!_gameVersions.ContainsKey(game.Id))
          {
            _gameVersions.Add(entry.GameId, new List<GameVersion>());
          }
          _gameVersions[game.Id].Add(game);

          foreach (var platform in version.Platforms)
          {
            if (!_platformGames.ContainsKey(platform))
            {
              _platformGames.Add(platform, new List<GameVersion>());
            }
            _platformGames[platform].Add(game);
          }
        }
      }
    }

    private async Task InitializeAsync()
    {
      await InitializeGamesAsync();

      foreach (var (platform, launcher) in _launchers.Select(x => (x.Key, x.Value)))
      {
        if (_platformGames[platform].Any())
        {
          launcher.Refresh();
          foreach (var installedGame in launcher.Games)
          {
            var game = _platformGames[platform].FirstOrDefault(g => g.Title == installedGame.Name);
            if (game != null)
            {
              var installation = new GameInstallation(installedGame.InstallDir, game.ExecutableRelativePath, platform, installedGame.LaunchString);

              if (_installations.TryGetValue(game.Id, out var installations))
              {
                installations.Add(installation);
              }
              else
              {
                _installations.Add(game.Id, new List<IGameInstallation> { installation });
              }
            }
          }
        }
      }
    }

    public static async Task<IGameRegistry> CreateAsync(IRegistrySource registrySource)
    {
      var registry = new GameRegistry(registrySource);
      await registry.InitializeAsync();
      return registry;
    }
  }
}
