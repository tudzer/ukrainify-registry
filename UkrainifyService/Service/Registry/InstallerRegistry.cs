﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal class InstallerRegistry : IInstallerRegistry
  {
    private readonly IPackageRegistry _packageRegistry;
    private readonly IPackageInstallerFactory _installerFactory;

    private readonly Dictionary<Tuple<IGameInstallation, ILocalization>, IPackageInstaller> _installers =
      new Dictionary<Tuple<IGameInstallation, ILocalization>, IPackageInstaller>();

    public InstallerRegistry(IPackageRegistry packageRegistry, IPackageInstallerFactory installerFactory)
    {
      _packageRegistry = packageRegistry;
      _installerFactory = installerFactory;
    }

    public async Task<IPackageInstaller> GetInstallerAsync(IGameInstallation gameInstallation, ILocalization localization)
    {
      var key = Tuple.Create(gameInstallation, localization);
      if (_installers.TryGetValue(key, out var existingInstaller))
      {
        return existingInstaller;
      }

      if (gameInstallation.Platform == GamePlatform.Custom && !localization.SupportedPlatforms.Contains(GamePlatform.Custom))
      {
        var compatibleInstaller = await FindCompatibleInstaller(gameInstallation.BasePath, localization);
        if (compatibleInstaller != null)
        {
          _installers.Add(key, compatibleInstaller);
        }

        return compatibleInstaller;
      }

      var package = await _packageRegistry.GetPackageAsync(localization, gameInstallation.Platform);
      var installer = _installerFactory.Create(gameInstallation.BasePath, package);
      _installers.Add(key, installer);

      return installer;
    }

    private async Task<IPackageInstaller> FindCompatibleInstaller(string gameInstallationPath, ILocalization localization)
    {
      foreach (var packageInfo in localization.Packages.Values)
      {
        var package = await _packageRegistry.GetPackageAsync(packageInfo);
        var installer = _installerFactory.Create(gameInstallationPath, package);
        if (installer.IsCompatible)
        {
          return installer;
        }
      }

      return null;
    }
  }
}
