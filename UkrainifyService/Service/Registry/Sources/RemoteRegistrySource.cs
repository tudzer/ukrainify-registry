﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ukrainify.L10n
{
  internal class RemoteRegistrySource : IRegistrySource
  {
    private ICollection<GameEntry> _entries;

    public async Task<ICollection<GameEntry>> GetEntriesAsync()
    {
      if (_entries != null) return _entries;

      using (var client = new HttpClient())
      {
        var registryUrl = Properties.Settings.Default.RemoteRegistryUrl;
        var response = await client.GetAsync(registryUrl);
        if (response.IsSuccessStatusCode)
        {
          var registryData = await response.Content.ReadAsStringAsync();

          _entries = JsonConvert.DeserializeObject<GameEntry[]>(registryData);
          Trace.WriteLine($"Loaded {_entries.Count} entries from remote registry:\n{registryUrl}");
        }
        else
        {
          throw new Exception($"Failed to download registry file: {response.ReasonPhrase}");
        }
      }

      return _entries;
    }
  }
}
