﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ukrainify.L10n
{
  internal class LocalRegistrySource : IRegistrySource
  {
    private ICollection<GameEntry> _entries;

    public Task<ICollection<GameEntry>> GetEntriesAsync()
    {
      if (_entries != null) return Task.FromResult(_entries);

      #if DEBUG
      var registryPath = Properties.Settings.Default.DebugLocalRegistryPath;
      #else
      var registryPath = Properties.Settings.Default.LocalRegistryPath;
      #endif
      var resolvedPath = Environment.ExpandEnvironmentVariables(registryPath);
      var registryData = File.ReadAllText(resolvedPath);
      _entries = JsonConvert.DeserializeObject<GameEntry[]>(registryData);
      Trace.WriteLine($"Loaded {_entries.Count} entries from local registry:\n{registryPath}");

      return Task.FromResult(_entries);
    }
  }
}
