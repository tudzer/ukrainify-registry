﻿using Ukrainify.L10n;

namespace Ukrainify
{
  internal class GameVersionEntry
  {
    public string Exe { get; set; }
    public string GameTitle { get; set; }
    public GamePlatform[] Platforms { get; set; }
  }
}
