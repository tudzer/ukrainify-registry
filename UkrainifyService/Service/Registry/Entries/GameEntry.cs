﻿namespace Ukrainify
{
  internal class GameEntry
  {
    public string GameId { get; set; }
    public GameVersionEntry[] Versions { get; set; }
    public LocalizationEntry[] Localizations { get; set; }
  }
}
