﻿using Ukrainify.L10n;

namespace Ukrainify
{
  internal class PackageEntry
  {
    public GamePlatform[] Platforms { get; set; }
    public PackageFormat Format { get; set; }
    public PackageSourceType Source { get; set; }
    public string Uri { get; set; }
  }
}
