﻿namespace Ukrainify
{
  internal class LocalizationEntry
  {
    public string LocalizationId { get; set; }
    public PackageEntry[] Packages { get; set; }
  }
}
