﻿using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Ukrainify.CommandProcessing;
using Ukrainify.L10n;
using Ukrainify.L10n.Integrity;

namespace Ukrainify
{
  internal static class DependencyInjection
  {
    public static async Task<IContainer> CreateContainerAsync()
    {
      #if DEBUG
      var registrySource = new LocalRegistrySource();
      #else
      var registrySource = new RemoteRegistrySource();
      #endif

      var localizationRegistry = await LocalizationRegistry.CreateAsync(registrySource);
      var gameRegistry = await GameRegistry.CreateAsync(registrySource);

      var builder = new ContainerBuilder();

      // Register hash algorithm
      builder.RegisterType<Md5HashCalculator>().As<IHashCalculator>().SingleInstance();

      // Register file digest providers
      builder.RegisterType<LastModifiedTimeDigestProvider>().AsSelf().SingleInstance();
      builder.RegisterType<SizeDigestProvider>().AsSelf().SingleInstance();
      builder.RegisterType<HashDigestProvider>().AsSelf().SingleInstance();
      builder.RegisterType<FileDigestProvider>().As<IFileDigestProvider<IFileDigest>>().SingleInstance();

      // Register factories
      builder.RegisterType<BackupStorageFactory>().As<IBackupStorageFactory>().SingleInstance();
      builder.RegisterType<LocalizedFileRegistryFactory>().As<ILocalizedFileRegistryFactory>().SingleInstance();

      builder.RegisterType<DefaultFileIntegrityVerificationStrategy>().As<IFileIntegrityVerificationStrategy>().SingleInstance();
      builder.RegisterType<LocalizationIntegrityVerifierFactory>().As<ILocalizationIntegrityVerifierFactory>().SingleInstance();


      builder.RegisterType<PackageFactory>().As<IPackageFactory>().SingleInstance();

      builder.RegisterType<LocalizationStateFactory>().As<ILocalizationStateFactory>().SingleInstance();
      builder.RegisterType<PackageInstallerContextFactory>().As<IPackageInstallerContextFactory>().SingleInstance();

      builder.RegisterType<ProprietaryPackageInstallerFactory>().AsSelf().SingleInstance();  
      builder.RegisterType<CustomPackageInstallerFactory>().AsSelf().SingleInstance();  
      builder.RegisterType<ArchivePackageInstallerFactory>().AsSelf().SingleInstance();

      builder.RegisterType<PackageInstallerFactory>().As<IPackageInstallerFactory>().SingleInstance();

      // Register package retrieval strategies
      builder.RegisterType<CdnPackageDownloadingStrategy>().AsSelf().SingleInstance();
      builder.RegisterType<GoogleDrivePackageDownloadingStrategy>().AsSelf().SingleInstance();
      builder.RegisterType<LocalPackageDownloadingStrategy>().AsSelf().SingleInstance();

      // Register package loading strategies
      builder.RegisterType<CustomPackageLoadingStrategy>().AsSelf().SingleInstance();
      builder.RegisterType<CommonPackageLoadingStrategy>().AsSelf().SingleInstance();

      // Register factories
      builder.RegisterType<PackageFactory>().As<IPackageFactory>().SingleInstance();
      builder.RegisterType<PackageInstallerFactory>().As<IPackageInstallerFactory>().SingleInstance();

      // Register loaders
      builder.RegisterType<PackageDownloader>().As<IPackageDownloader>().SingleInstance();
      //builder.RegisterType<PackageInstallerFactory>().As<IPackageInstallerFactory>().SingleInstance();

      // Register registries
      builder.RegisterType<PackageRegistry>().As<IPackageRegistry>().SingleInstance();
      builder.RegisterType<InstallerRegistry>().As<IInstallerRegistry>().SingleInstance();
      builder.RegisterInstance(localizationRegistry).As<ILocalizationRegistry>().SingleInstance();
      builder.RegisterInstance(gameRegistry).As<IGameRegistry>().SingleInstance();

      // Register installer facade
      builder.RegisterType<InstallerFacade>().As<IInstallerFacade>().SingleInstance();

      // Register command processing
      RegisterCommandTypes(builder);

      builder.RegisterType<CommandExecutor>().As<ICommandExecutor>().SingleInstance();
      builder.RegisterType<ConsoleCommandProcessor>().As<ICommandProcessor>().SingleInstance();

      var container = builder.Build();
      return container;
    }

    private static void RegisterCommandTypes(ContainerBuilder builder)
    {
      builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
        .Where(type => type.GetCustomAttribute<CommandName>() != null)
        .AsSelf();
    }
  }
}
