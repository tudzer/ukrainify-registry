﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;
using Autofac;
using Ukrainify.CommandProcessing;

namespace Ukrainify
{
  internal class Program
  {
    private static async Task Main()
    {
      try
      {
        //while (!Debugger.IsAttached) System.Threading.Thread.Sleep(100);

        Trace.Listeners.Add(new TextWriterTraceListener(Environment.ExpandEnvironmentVariables(Properties.Settings.Default.LogFilePath))                                                                                                            );
        Trace.AutoFlush = true;

        var container = await DependencyInjection.CreateContainerAsync();
        var commandProcessor = container.Resolve<ICommandProcessor>();
        await commandProcessor.StartListeningAsync();
      }
      catch (Exception e)
      {
        Trace.WriteLine($"Failed to initialize service:\n{e}");
      }
    }
  }
}
