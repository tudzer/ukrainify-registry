﻿using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal class InstallerFacade : IInstallerFacade
  {
    private readonly IGameRegistry _gameRegistry;
    private readonly IInstallerRegistry _installerRegistry;
    private readonly ILocalizationRegistry _localizationRegistry;

    public InstallerFacade(IGameRegistry gameRegistry, ILocalizationRegistry localizationRegistry, IInstallerRegistry installerRegistry)
    {
      _gameRegistry = gameRegistry;
      _localizationRegistry = localizationRegistry;
      _installerRegistry = installerRegistry;
    }

    public Task<IPackageInstaller> GetInstallerAsync(string gameId, string localizationId, GamePlatform platform)
    {
      var gameInstallation = _gameRegistry.GetInstallation(gameId, platform);
      var localization = _localizationRegistry.GetLocalization(gameId, localizationId);

      return _installerRegistry.GetInstallerAsync(gameInstallation, localization);
    }

    public Task<IPackageInstaller> GetInstallerAsync(string gameId, string localizationId, string gameInstallationPath)
    {
      var gameInstallation = _gameRegistry.GetInstallation(gameId, gameInstallationPath);
      var localization = _localizationRegistry.GetLocalization(gameId, localizationId);

      return _installerRegistry.GetInstallerAsync(gameInstallation, localization);
    }
  }
}
