﻿namespace Ukrainify.L10n
{
  internal class GameVersion
  {
    public string Id { get; set; }

    public string Title { get; set; }

    public string ExecutableRelativePath { get; set; }
  }
}
