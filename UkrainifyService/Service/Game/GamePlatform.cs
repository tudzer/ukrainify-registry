﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ukrainify.L10n
{
  [JsonConverter(typeof(StringEnumConverter))]
  public enum GamePlatform
  {
    [EnumMember(Value = "steam")]
    Steam,
    [EnumMember(Value = "gog")]
    Gog,
    [EnumMember(Value = "epic")]
    Epic,
    [EnumMember(Value = "custom")]
    Custom
  }
}
