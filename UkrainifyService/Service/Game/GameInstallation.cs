﻿using System.Diagnostics;
using System.IO;

namespace Ukrainify.L10n
{
  internal class GameInstallation : IGameInstallation
  {
    public string BasePath { get; set; }
    public GamePlatform Platform { get; }

    private readonly string _launchCommand;
    private readonly string _launchArgs;
    private readonly string _gameExecutablePath;

    public GameInstallation(string path, string executableRelativePath = null, GamePlatform platform = GamePlatform.Custom, string launchString = null)
    {
      BasePath = path;
      Platform = platform;

      if (launchString != null)
      {
        (_launchCommand, _launchArgs) = ParseLaunchString(launchString);
      }

      if (!string.IsNullOrEmpty(executableRelativePath))
      {
        _gameExecutablePath = Path.Combine(path, executableRelativePath);
      }
    }

    public void Launch()
    {
      if (_launchCommand != null)
      {
        Process.Start(_launchCommand, _launchArgs);
      }
      else if (!string.IsNullOrEmpty(_gameExecutablePath))
      {
        Launch(_gameExecutablePath);
      }
    }

    public void Launch(string executablePath)
    {
      Process.Start(new ProcessStartInfo
      {
        FileName = executablePath,
        WorkingDirectory = Path.GetDirectoryName(executablePath) ?? string.Empty,
      });
    }

    public bool Equals(IGameInstallation other)
    {
      return other != null && BasePath == other.BasePath;
    }

    private static (string, string) ParseLaunchString(string launchString)
    {
      var chars = launchString.ToCharArray();
      var inQuote = false;
      for (var index = 0; index < chars.Length; index++)
      {
        if (chars[index] == '"') inQuote = !inQuote;
        if (inQuote || chars[index] != ' ') continue;
        var command = launchString.Substring(0, index);
        var args = launchString.Substring(index);
        return (command, args);
      }

      return (launchString, null);
    }
  }
}
