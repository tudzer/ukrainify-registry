﻿using System.Text;

namespace Ukrainify
{
  internal static class HashUtils
  {
    public static string ToHexString(this byte[] bytes)
    {
      var sb = new StringBuilder();
      foreach (var b in bytes)
      {
        sb.Append(b.ToString("X2"));
      }

      return sb.ToString();
    }
  }
}
