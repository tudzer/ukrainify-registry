﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Ukrainify
{
  internal static class EnumUtils
  {
    public static IEnumerable<T> GetAllValues<T>() where T : Enum
    {
      return Enum.GetValues(typeof(T)).Cast<T>();
    }

    public static string GetStringValue<T>(this T enumValue)
    {
      var enumType = typeof(T);
      var memberInfo = enumType.GetMember(enumValue.ToString());
      var attribute = memberInfo.FirstOrDefault()?.GetCustomAttributes(false).OfType<EnumMemberAttribute>().FirstOrDefault();
      return attribute?.Value;
    }

    public static T FromString<T>(string enumValue)
    {
      return (T)Enum.Parse(typeof(T), enumValue);
    }
  }
}
