﻿using System;
using System.Linq;
using System.Reflection;

namespace Ukrainify
{
    internal static class ReflectionUtils
    {
        public static T CreateInstance<T>(Assembly assembly, params object[] args) where T : class
        {
            var type = assembly.GetTypes().FirstOrDefault(x => typeof(T).IsAssignableFrom(x) && !x.IsAbstract);
            if (type == null)
            {
              throw new Exception($"No implementation of {typeof(T).Name} found.");
            }

            var instance = Activator.CreateInstance(type, args) as T;
            if (instance == null)
            {
              throw new Exception($"Failed to create an instance of {type.Name}.");
            }

            return instance;
        }
    }
}
