﻿using System.IO;

namespace Ukrainify
{
  internal static class PathUtils
  {
    public static bool IsFile(string path)
    {
      return File.Exists(path);
    }

    public static bool IsDirectory(string path)
    {
      return Directory.Exists(path);
    }

    public static string GetDeepestDirectoryName(string path)
    {
      var dir = IsFile(path) ? Path.GetDirectoryName(path) : path;
      return IsDirectory(dir) ? new DirectoryInfo(dir).Name : null;
    }
  }
}
