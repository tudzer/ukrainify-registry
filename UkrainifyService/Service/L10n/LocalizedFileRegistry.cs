﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ukrainify.L10n.Integrity;

namespace Ukrainify.L10n
{
  internal class LocalizedFileRegistry : ILocalizedFileRegistry
  {
    private const string RegistryFileName = "file_registry";

    public bool Empty => !_entries.Value.Any();
    public IEnumerable<ILocalizedFileEntry> Entries => _entries.Value;
    public IEnumerable<string> AddedFiles => _entries.Value.Where(entry => entry.Type == FileType.New).Select(entry => entry.RelativePath);

    private readonly string _hashStoragePath;
    private readonly Lazy<List<ILocalizedFileEntry>> _entries;

    public LocalizedFileRegistry(string basePath)
    {
      _hashStoragePath = Path.Combine(basePath, RegistryFileName);
      _entries = new Lazy<List<ILocalizedFileEntry>>(LoadFileEntries);
    }

    public void AddEntry(string relativeFilePath, FileType type, IFileDigest digest)
    {
      var entry = new LocalizedFileEntry(relativeFilePath, type, digest);
      _entries.Value.Add(entry);

      Save();
    }

    private void Save()
    {
      using (var storage = new StreamWriter(_hashStoragePath))
      {
        foreach (var entry in _entries.Value)
        {
          var value = $"{entry.Type}:{entry.RelativePath}:{entry.Digest.LastModifiedTime}:{entry.Digest.Size}:{entry.Digest.Hash}";
          storage.WriteLine(value);
        }
      }
    }

    private List<ILocalizedFileEntry> LoadFileEntries()
    {
      var entries = new List<ILocalizedFileEntry>();

      if (!File.Exists(_hashStoragePath)) return entries;

      foreach (var line in File.ReadLines(_hashStoragePath))
      {
        var properties = line.Split(':');

        var type = EnumUtils.FromString<FileType>(properties[0]);

        var relativePath = properties[1];
        var lastModifiedDate = Convert.ToInt64(properties[2]);
        var size = Convert.ToInt64(properties[3]);
        var hash = properties[4];
        var digest = new FileDigest(lastModifiedDate, size, hash);

        var entry = new LocalizedFileEntry(relativePath, type, digest);

        entries.Add(entry);
      }

      return entries;
    }
  }
}
