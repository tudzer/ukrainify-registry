﻿using System.IO;
using Ukrainify.L10n.Integrity;

namespace Ukrainify.L10n
{
  internal class LocalizationState : ILocalizationState
  {
    public string Path { get; }
    public ILocalizedFileRegistry LocalizedFileRegistry { get; }
    public IBackupStorage Backup { get; }
    public ILocalizationIntegrityVerifier Integrity { get; }

    public LocalizationState(string path, ILocalizedFileRegistry localizedFileRegistry, IBackupStorage backupStorage, ILocalizationIntegrityVerifier integrityVerifier)
    {
      LocalizedFileRegistry = localizedFileRegistry;
      Backup = backupStorage;
      Integrity = integrityVerifier;
      Path = path;
    }

    public void Clear()
    {
      var directoryInfo = new DirectoryInfo(Path);
      if (directoryInfo.Exists)
      {
        directoryInfo.Delete(true);
      }
    }
  }
}
