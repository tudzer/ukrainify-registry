﻿using Ukrainify.L10n.Integrity;

namespace Ukrainify.L10n
{
  internal class LocalizedFileEntry : ILocalizedFileEntry
  {
    public string RelativePath { get; }

    public FileType Type { get; }

    public IFileDigest Digest { get; }

    public LocalizedFileEntry(string relativePath, FileType type, IFileDigest digest)
    {
      RelativePath = relativePath;
      Type = type;
      Digest = digest;
    }
  }
}
