﻿using System.Collections.Generic;
using System.Linq;

namespace Ukrainify.L10n
{
  internal class Localization : ILocalization
  {
    public string GameId { get; }
    public string LocalizationId { get; }
    public ICollection<GamePlatform> SupportedPlatforms => _packages.Keys;
    public IReadOnlyDictionary<GamePlatform, PackageInfo> Packages => (IReadOnlyDictionary<GamePlatform, PackageInfo>)_packages;

    private readonly IDictionary<GamePlatform, PackageInfo> _packages = new Dictionary<GamePlatform, PackageInfo>();

    public Localization(string gameId, LocalizationEntry entry)
    {
      GameId = gameId;
      LocalizationId = entry.LocalizationId;

      var packageInfoCache = new Dictionary<(PackageSourceType, string), PackageInfo>();

      foreach (var platform in EnumUtils.GetAllValues<GamePlatform>())
      {
        if (platform == GamePlatform.Custom) continue;
        var package = entry.Packages.FirstOrDefault(p => p.Platforms.Contains(platform));
        if (package == null) continue;

        var key = (package.Source, package.Uri);

        if (packageInfoCache.TryGetValue(key, out var cachedPackageInfo))
        {
          _packages.Add(platform, cachedPackageInfo);
        }
        else
        {
          var packageSource = new PackageSource(package.Source, package.Uri);
          var packageInfo = new PackageInfo(this, package.Format, package.Platforms, packageSource);
          packageInfoCache.Add(key, packageInfo);
          _packages.Add(platform, packageInfo);
        }
      }
    }
  }
}
