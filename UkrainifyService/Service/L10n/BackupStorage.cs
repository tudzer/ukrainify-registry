﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace Ukrainify.L10n
{
  public class BackupStorage : IBackupStorage
  {
    private const string BackupFileName = "backup";

    private readonly string _backupFilePath;
    private readonly string _gameInstallationPath;

    public bool Empty => !File.Exists(_backupFilePath);

    public BackupStorage(string basePath, string gameInstallationPath)
    {
      _backupFilePath = Path.Combine(basePath, BackupFileName);
      _gameInstallationPath = gameInstallationPath;
    }

    public void StoreFiles(IEnumerable<string> relativeFilePaths)
    {
      using (var archive = CreateBackup())
      {
        foreach (var relativeFilePath in relativeFilePaths)
        {
          var filePath = Path.Combine(_gameInstallationPath, relativeFilePath);
          archive.CreateEntryFromFile(filePath, relativeFilePath, CompressionLevel.NoCompression);
        }
      }
    }

    public void StoreFile(string relativeFilePath)
    {
      var filePath = Path.Combine(_gameInstallationPath, relativeFilePath);

      using (var archive = UpdateBackup())
      {
        archive.CreateEntryFromFile(filePath, relativeFilePath, CompressionLevel.Fastest);
      }
    }

    public void Restore()
    {
      if (Empty) return;

      using (var archive = ReadBackup())
      {
        foreach (var entry in archive.Entries)
        {
          RestoreEntry(entry);
        }
      }
    }

    public void Restore(IEnumerable<string> relativeFilePaths)
    {
      if (Empty) return;

      using (var archive = ReadBackup())
      {
        foreach (var relativeFilePath in relativeFilePaths)
        {
          var entry = archive.GetEntry(relativeFilePath);
          if (entry == null) continue;
          RestoreEntry(entry);
        }
      }
    }

    private void RestoreEntry(ZipArchiveEntry entry)
    {
      var filePath = Path.Combine(_gameInstallationPath, entry.FullName);
      if (File.Exists(filePath)) File.Delete(filePath);
      entry.ExtractToFile(filePath);
    }

    private ZipArchive CreateBackup()
    {
      var stream = new FileStream(_backupFilePath, FileMode.Create, FileAccess.ReadWrite);
      return new ZipArchive(stream, ZipArchiveMode.Create);
    }

    private ZipArchive UpdateBackup()
    {
      var stream = new FileStream(_backupFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
      return new ZipArchive(stream, ZipArchiveMode.Update);
    }

    private ZipArchive ReadBackup()
    {
      var stream = File.OpenRead(_backupFilePath);
      return new ZipArchive(stream, ZipArchiveMode.Read);
    }
  }
}
