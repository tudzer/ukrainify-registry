﻿namespace Ukrainify.L10n.Integrity
{
  internal class FileDigest : IFileDigest
  {
    public long LastModifiedTime { get; }

    public long Size { get; }

    public string Hash { get; }

    public FileDigest(long lastModifiedTime, long size, string hash)
    {
      LastModifiedTime = lastModifiedTime;
      Size = size;
      Hash = hash;
    }
  }
}
