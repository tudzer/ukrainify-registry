﻿namespace Ukrainify.L10n.Integrity
{
  internal class DefaultFileIntegrityVerificationStrategy : IFileIntegrityVerificationStrategy
  {
    private readonly LastModifiedTimeDigestProvider _lastModifiedTimeDigestProvider; 
    private readonly SizeDigestProvider _sizeDigestProvider; 
    private readonly HashDigestProvider _hashDigestProvider;

    public DefaultFileIntegrityVerificationStrategy(LastModifiedTimeDigestProvider lastModifiedTimeDigestProvider, SizeDigestProvider sizeDigestProvider, HashDigestProvider hashDigestProvider)
    {
      _lastModifiedTimeDigestProvider = lastModifiedTimeDigestProvider;
      _sizeDigestProvider = sizeDigestProvider;
      _hashDigestProvider = hashDigestProvider;
    }

    public bool Execute(IFileDigest digest, string filePath)
    {
      var lastModifiedTime = _lastModifiedTimeDigestProvider.GetDigest(filePath);
      if (lastModifiedTime == digest.LastModifiedTime) return false;

      var size = _sizeDigestProvider.GetDigest(filePath);
      if (size != digest.Size) return true;

      var hash = _hashDigestProvider.GetDigest(filePath);
      return !hash.Equals(digest.Hash);
    }
  }
}
