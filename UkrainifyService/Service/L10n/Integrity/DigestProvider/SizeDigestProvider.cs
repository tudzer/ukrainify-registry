﻿using System.IO;

namespace Ukrainify.L10n.Integrity
{
  internal class SizeDigestProvider : IFileDigestProvider<long>
  {
    public long GetDigest(string filePath)
    {
      return new FileInfo(filePath).Length;
    }
  }
}
