﻿using System.IO;

namespace Ukrainify.L10n.Integrity
{
  internal class LastModifiedTimeDigestProvider : IFileDigestProvider<long>
  {
    public long GetDigest(string filePath)
    {
      return File.GetLastWriteTimeUtc(filePath).Ticks;
    }
  }
}
