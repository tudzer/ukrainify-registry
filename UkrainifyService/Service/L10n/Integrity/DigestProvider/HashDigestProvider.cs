﻿using System.IO;

namespace Ukrainify.L10n.Integrity
{
  internal class HashDigestProvider : IFileDigestProvider<string>
  {
    private readonly IHashCalculator _hashCalculator;

    public HashDigestProvider(IHashCalculator hashCalculator)
    {
      _hashCalculator = hashCalculator;
    }

    public string GetDigest(string filePath)
    {
      var fileBytes = File.ReadAllBytes(filePath);

      return _hashCalculator.Calculate(fileBytes);
    }
  }
}
