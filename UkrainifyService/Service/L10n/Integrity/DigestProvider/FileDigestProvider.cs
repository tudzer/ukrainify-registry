﻿namespace Ukrainify.L10n.Integrity
{
  internal class FileDigestProvider: IFileDigestProvider<IFileDigest>
  {
    private readonly LastModifiedTimeDigestProvider _lastModifiedTimeDigestProvider;
    private readonly SizeDigestProvider _sizeDigestProvider;
    private readonly HashDigestProvider _hashDigestProvider;

    public FileDigestProvider(LastModifiedTimeDigestProvider lastModifiedTimeDigestProvider, SizeDigestProvider sizeDigestProvider, HashDigestProvider hashDigestProvider)
    {
      _lastModifiedTimeDigestProvider = lastModifiedTimeDigestProvider;
      _sizeDigestProvider = sizeDigestProvider;
      _hashDigestProvider = hashDigestProvider;
    }

    public IFileDigest GetDigest(string filePath)
    {
      var lastModifiedTime = _lastModifiedTimeDigestProvider.GetDigest(filePath);
      var size = _sizeDigestProvider.GetDigest(filePath);
      var hash = _hashDigestProvider.GetDigest(filePath);

      return new FileDigest(lastModifiedTime, size, hash);
    }
  }
}
