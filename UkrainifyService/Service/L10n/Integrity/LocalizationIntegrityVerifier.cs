﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Ukrainify.L10n.Integrity
{
  internal class LocalizationIntegrityVerifier : ILocalizationIntegrityVerifier
  {
    private readonly string _gameInstallationPath;
    private readonly ILocalizedFileRegistry _localizedFileRegistry;
    private readonly IFileDigestProvider<IFileDigest> _digestProvider;
    private readonly IFileIntegrityVerificationStrategy _integrityVerificationStrategy;

    public LocalizationIntegrityVerifier(
      string gameInstallationPath,
      ILocalizedFileRegistry localizedFileRegistry,
      IFileDigestProvider<IFileDigest> digestProvider,
      IFileIntegrityVerificationStrategy integrityVerificationStrategy
      )
    {
      _gameInstallationPath = gameInstallationPath;
      _localizedFileRegistry = localizedFileRegistry;
      _digestProvider = digestProvider;
      _integrityVerificationStrategy = integrityVerificationStrategy;
    }

    public bool IsModified => _localizedFileRegistry.Entries.Any(IsFileModified);

    public void RegisterFile(string relativeFilePath, FileType type)
    {
      var filePath = Path.Combine(_gameInstallationPath, relativeFilePath);
      var digest = _digestProvider.GetDigest(filePath);
      _localizedFileRegistry.AddEntry(relativeFilePath, type, digest);
    }

    public IEnumerable<string> UnchangedFiles => _localizedFileRegistry.Entries
      .Where(fileEntry => !IsFileModified(fileEntry))
      .Select(fileEntry => fileEntry.RelativePath);

    private bool IsFileModified(ILocalizedFileEntry fileEntry)
    {
      var filePath = Path.Combine(_gameInstallationPath, fileEntry.RelativePath);

      return !File.Exists(filePath) || _integrityVerificationStrategy.Execute(fileEntry.Digest, filePath);
    }
  }
}
