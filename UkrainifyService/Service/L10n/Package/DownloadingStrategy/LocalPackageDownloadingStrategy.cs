﻿using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal class LocalPackageDownloadingStrategy : IPackageDownloadingStrategy
  {
    public Task<string> Execute(string filePath, string extension)
    {
      return Task.FromResult(filePath);
    }
  }
}
