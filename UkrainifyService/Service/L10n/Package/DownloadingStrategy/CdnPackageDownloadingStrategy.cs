﻿using System.Net.Http;
using System;
using System.Threading.Tasks;
using Ukrainify.L10n.Package.LoadingStrategies;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json.Linq;

namespace Ukrainify.L10n
{
  internal class CdnPackageDownloadingStrategy : RemotePackageDownloadingStrategy
  {
    private static readonly string Username = Properties.Settings.Default.StorageUsername;
    private static readonly string Password = Properties.Settings.Default.StoragePassword;
    private static readonly string BaseUrl = Properties.Settings.Default.StorageBaseUrl;

    private DateTime _lastTokenRefresh;
    private string _token;

    public CdnPackageDownloadingStrategy(IHashCalculator hashCalculator) : base(hashCalculator)
    {
      _lastTokenRefresh = DateTime.MinValue;
    }

    private async Task EnsureLoggedInAsync()
    {
      if (_token == null || (DateTime.Now - _lastTokenRefresh).TotalMinutes >= 30)
      {
        var loginUri = $"{BaseUrl}/~/action/storage/auth/login/";
        var requestBody = JsonConvert.SerializeObject(new { username = Username, password = Password });
        var content = new StringContent(requestBody, Encoding.UTF8, "application/json");

        var response = await httpClient.PostAsync(loginUri, content);
        response.EnsureSuccessStatusCode();

        var responseString = await response.Content.ReadAsStringAsync();
        dynamic responseData = JObject.Parse(responseString);
        _token = responseData.callback.token.ToString();
        _lastTokenRefresh = DateTime.Now;
        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Storage-Token", _token);
      }
    }

    public override async Task<string> Execute(string uri, string extension)
    {
      await EnsureLoggedInAsync();
      var fileUrl = $"{BaseUrl}/Packages/{uri}";
      return await base.Execute(fileUrl, extension);
    }
  }
}
