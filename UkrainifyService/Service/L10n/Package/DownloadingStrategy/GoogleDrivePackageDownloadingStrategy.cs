﻿using System.Threading.Tasks;
using Ukrainify.L10n.Package.LoadingStrategies;

namespace Ukrainify.L10n
{
  internal class GoogleDrivePackageDownloadingStrategy : RemotePackageDownloadingStrategy
  {
    private const string DownloadUrl = "https://drive.google.com/uc?export=download&id=";

    public GoogleDrivePackageDownloadingStrategy(IHashCalculator hashCalculator) : base(hashCalculator) { }

    public override Task<string> Execute(string uri, string extension)
    {
      var fileUrl = DownloadUrl + uri;
      return base.Execute(fileUrl, extension);
    }
  }
}
