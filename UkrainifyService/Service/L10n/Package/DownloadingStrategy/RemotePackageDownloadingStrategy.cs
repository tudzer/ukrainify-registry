﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ukrainify.L10n.Package.LoadingStrategies
{
  internal class RemotePackageDownloadingStrategy : IPackageDownloadingStrategy
  {
    private const string PackagesDirectoryName = "LocalizationPackages";

    protected readonly HttpClient httpClient = new HttpClient();

    private readonly IHashCalculator _hashCalculator;
    private readonly string _packagesPath = Path.Combine(Directory.GetCurrentDirectory(), PackagesDirectoryName);


    public RemotePackageDownloadingStrategy(IHashCalculator hashCalculator)
    {
      _hashCalculator = hashCalculator;
      Directory.CreateDirectory(_packagesPath);
    }

    protected async Task<byte[]> DownloadPackage(string url)
    {
      using (var response = await httpClient.GetAsync(url))
      {
        if (!response.IsSuccessStatusCode) throw new Exception($"Failed to download file: {response.ReasonPhrase}");

        var bytes = await response.Content.ReadAsByteArrayAsync();
        return bytes;
      }
    }

    protected string SavePackageToFile(byte[] packageBytes, string extension)
    {
      var packageHash = _hashCalculator.Calculate(packageBytes);
      var packageName = $"{packageHash}.{extension}";
      var path = Path.Combine(_packagesPath, packageName);

      using (var file = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
      {
        file.Write(packageBytes, 0, packageBytes.Length);
      }

      return path;
    }

    public virtual async Task<string> Execute(string url, string extension)
    {
      var packageBytes = await DownloadPackage(url);
      var packageFilePath = SavePackageToFile(packageBytes, extension);
      return packageFilePath;
    }
  }
}
