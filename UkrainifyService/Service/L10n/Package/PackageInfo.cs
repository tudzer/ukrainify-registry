﻿using System.Collections.Generic;

namespace Ukrainify.L10n
{
  internal class PackageInfo
  {
    public ILocalization Localization { get; }

    public PackageFormat Format { get; }

    public IEnumerable<GamePlatform> Platforms { get; }

    public PackageSource Source { get; }

    public PackageInfo(ILocalization localization, PackageFormat format, IEnumerable<GamePlatform> platforms, PackageSource source)
    {
      Localization = localization;
      Format = format;
      Platforms = platforms;
      Source = source;
    }
  }
}
