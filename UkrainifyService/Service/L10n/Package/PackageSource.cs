﻿namespace Ukrainify.L10n
{
  internal class PackageSource
  {
    public PackageSourceType Type { get; }

    public string Uri { get; }

    public PackageSource(PackageSourceType type, string uri)
    {
      Type = type;
      Uri = uri;
    }

    public bool Equals(PackageSource other)
    {
      return other.Type == Type && other.Uri == Uri;
    }
  }
}
