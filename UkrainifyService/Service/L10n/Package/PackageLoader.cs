﻿namespace Ukrainify.L10n
{
  internal class PackageLoader : IPackageLoader
  {
    private readonly CustomPackageLoadingStrategy _customStrategy;
    private readonly CommonPackageLoadingStrategy _commonStrategy;

    public PackageLoader(CustomPackageLoadingStrategy customStrategy, CommonPackageLoadingStrategy commonStrategy)
    {
      _customStrategy = customStrategy;
      _commonStrategy = commonStrategy;
    }

    public byte[] LoadPackage(ILocalizationPackage package)
    {
      var packageLoadingStrategy = package.Format == PackageFormat.Custom
        ? _customStrategy
        : _commonStrategy;

      return packageLoadingStrategy.Execute(package.FilePath);
    }
  }
}
