﻿namespace Ukrainify.L10n
{
  internal class LocalizationPackage : ILocalizationPackage
  {
    public string FilePath { get; }
    public PackageFormat Format { get; }

    public LocalizationPackage(string filePath, PackageFormat format)
    {
      FilePath = filePath;
      Format = format;
    } 
  }
}
