﻿using System.IO.Compression;
using System.IO;
using System.Linq;

namespace Ukrainify.L10n
{
  internal class CustomPackageLoadingStrategy : CommonPackageLoadingStrategy
  {
    public override byte[] Execute(string filePath)
    {
      var bytes = base.Execute(filePath);
      return TryUnarchive(bytes);
    }

    private static byte[] TryUnarchive(byte[] bytes)
    {
      try
      {
        var packageStream = new MemoryStream(bytes);
        using (var zip = new ZipArchive(packageStream, ZipArchiveMode.Read))
        {
          var entry = zip.Entries.Single();
          using (var output = new MemoryStream())
          using (var entryStream = entry.Open())
          {
            entryStream.CopyTo(output);
            return output.ToArray();
          }
        }
      }
      catch
      {
        return bytes;
      }
    }
  }
}
