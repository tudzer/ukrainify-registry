﻿using System.IO;

namespace Ukrainify.L10n
{
  internal class CommonPackageLoadingStrategy : IPackageLoadingStrategy
  {
    public virtual byte[] Execute(string filePath)
    {
      return File.ReadAllBytes(filePath);
    }
  }
}
