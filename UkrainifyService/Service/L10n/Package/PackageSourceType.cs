﻿using System.Runtime.Serialization;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace Ukrainify.L10n
{
  [JsonConverter(typeof(StringEnumConverter))]
  internal enum PackageSourceType
  {
    [EnumMember(Value = "cdn")]
    Cdn,
    [EnumMember(Value = "google_drive")]
    GoogleDrive,
    [EnumMember(Value = "local")]
    Local
  }
}
