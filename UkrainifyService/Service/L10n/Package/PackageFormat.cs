﻿using System.Runtime.Serialization;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace Ukrainify.L10n
{
  [JsonConverter(typeof(StringEnumConverter))]
  internal enum PackageFormat
  {
    [EnumMember(Value = "ulp")]
    Proprietary,
    [EnumMember(Value = "custom")]
    Custom,
    [EnumMember(Value = "zip")]
    Archive
  }
}
