﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ukrainify.L10n
{
  internal class PackageDownloader : IPackageDownloader
  {
    private readonly Dictionary<PackageSourceType, IPackageDownloadingStrategy> _strategies;

    public PackageDownloader(
      CdnPackageDownloadingStrategy cdnStrategy,
      GoogleDrivePackageDownloadingStrategy googleDriveStrategy,
      LocalPackageDownloadingStrategy localStrategy
      )
    {
      _strategies = new Dictionary<PackageSourceType, IPackageDownloadingStrategy>
      {
        { PackageSourceType.Cdn, cdnStrategy },
        { PackageSourceType.GoogleDrive, googleDriveStrategy },
        { PackageSourceType.Local, localStrategy }
      };
    }

    public Task<string> RetrievePackageAsync(PackageInfo packageInfo)
    {
      var packageExtension = packageInfo.GetStringValue();
      var packageRetrievalStrategy = _strategies[packageInfo.Source.Type];

      return packageRetrievalStrategy.Execute(packageInfo.Source.Uri, packageExtension);
    }
  }
}
