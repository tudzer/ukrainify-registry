﻿using System.Security.Cryptography;
using System.Text;

namespace Ukrainify
{
  public class Md5HashCalculator : IHashCalculator
  {
    public string Calculate(string input)
    {
      var inputBytes = Encoding.UTF8.GetBytes(input);
      return Calculate(inputBytes);
    }

    public string Calculate(byte[] inputBytes)
    {
      using (var md5 = MD5.Create())
      {
        var hashBytes = md5.ComputeHash(inputBytes);

        return hashBytes.ToHexString();
      }
    }
  }
}
