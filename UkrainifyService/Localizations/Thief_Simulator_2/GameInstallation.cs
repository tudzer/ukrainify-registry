﻿using System.IO;
using Ukrainify.Unity;

namespace Ukrainify
{
  public class GameInstallation : UnityGameInstallation
  {
    private const string GameDataDir = "Thief Simulator 2_Data";
    private const string TargetAssetsFile = "resources.assets";
    private const string TempFile = TargetAssetsFile + ".tmp";
    private const string BackupFile = TargetAssetsFile + ".backup";

    public string RelativeAssetsFilePath { get; }
    public string AssetsFilePath { get; }
    public string BackupFilePath { get; }
    public string TempFilePath { get; }


    public GameInstallation(string path) : base(path, GameDataDir)
    {
      RelativeAssetsFilePath = Path.Combine(GameDataDir, TargetAssetsFile);
      AssetsFilePath = Path.Combine(GameDataPath, TargetAssetsFile);
      BackupFilePath = Path.Combine(GameDataPath, BackupFile);
      TempFilePath = Path.Combine(GameDataPath, TempFile);
    }
  }
}
