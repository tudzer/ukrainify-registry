﻿using AssetsTools.NET.Extra;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Ukrainify.PackageInstallers;
using Ukrainify.Unity;

namespace Ukrainify
{
  public class CustomPackageInstaller : PackageInstaller<CustomPackageInstallerContext>
  {
    private const string UkrainianLanguage = "Українська";
    private const string LocalizationFile = "LocalizationFile";

    private readonly GameInstallation _gameInstallation;

    public CustomPackageInstaller(CustomPackageInstallerContext context, ILocalizationState localizationState) : base(context, localizationState)
    {
      _gameInstallation = new GameInstallation(context.GameInstallationPath);
    }

    public override void Install()
    {
      var assembly = Assembly.GetExecutingAssembly();

      var tpk = assembly.GetResource("tpk");
      var assetFileContext = new AssetsFileContext(_gameInstallation, tpk, _gameInstallation.AssetsFilePath);

      var localizationFile = assetFileContext.GetAsset(AssetClassID.TextAsset, LocalizationFile);
      var localizationContent = assetFileContext.GetAssetContent(localizationFile);
      var localizationScript = localizationContent["m_Script"];
      var json = localizationScript.AsString;
      var localization = JsonConvert.DeserializeObject<LocalizationFile>(json);

      LocalizationState.Backup.StoreFile(_gameInstallation.RelativeAssetsFilePath);

      byte languageId = 0;
      foreach (var term in localization.AllText)
      {
        var translation = term.Translations.FirstOrDefault(x => x.Word == UkrainianLanguage);
        if (translation == null) continue;
        languageId = translation.Language;
        break;
      }

      var localizationStream = assembly.GetResource("localization");
      Dictionary<string, string> uaTerms;

      using (var reader = new StreamReader(localizationStream))
      {
        var translation = reader.ReadToEnd();
        uaTerms = JsonConvert.DeserializeObject<Dictionary<string, string>>(translation);
      }

      foreach (var term in localization.AllText)
      {
        var translation = term.Translations.First(x => x.Language == languageId);
        if (uaTerms.TryGetValue(term.Id, out var value))
        {
          translation.Word = value;
        }
      }

      localizationScript.AsString = JsonConvert.SerializeObject(
        localization,
        new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }
      );
      localizationFile.SetNewData(localizationContent);

      assetFileContext.SaveAndDispose();

      LocalizationState.Integrity.RegisterFile(_gameInstallation.RelativeAssetsFilePath, FileType.Existing);
    }
  }
}
