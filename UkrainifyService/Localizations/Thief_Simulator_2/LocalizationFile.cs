﻿using Newtonsoft.Json;

namespace Ukrainify
{
  internal class LocalizationFile
  {
    public Term[] AllText { get; set; }
  }

  internal class Term
  {
    [JsonProperty("ID")]

    public string Id { get; set; }

    [JsonProperty("enumID")]
    public int EnumId { get; set; }

    public Translation[] Translations { get; set; }
  }

  internal class Translation
  {
    public byte Language { get; set; }

    public string Word { get; set; }
  }
}
