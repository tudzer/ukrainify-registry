﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using Ukrainify.PackageInstallers;

namespace Ukrainify
{
  public class CustomPackageInstaller : PackageInstaller<CustomPackageInstallerContext>
  {
    private const string LocalizationFolder = "ukr";

    public override bool IsCompatible => !string.IsNullOrEmpty(_executableRelativePath);

    private readonly Dictionary<string, string> _supportedVersions = new Dictionary<string, string>()
    {
      { "steam", "Postal Plus.exe" },
      { "gog", "Postal.exe" },
    };

    private readonly string _resourceName;
    private readonly string _localizationPath;
    private readonly string _executableRelativePath;


    public CustomPackageInstaller(CustomPackageInstallerContext context, ILocalizationState localizationState) : base(context, localizationState)
    {
      var version = _supportedVersions.FirstOrDefault(e => File.Exists(GetFullPath(e.Value)));
      if (version.Key == null) return;

      _resourceName = version.Key;
      _localizationPath = GetFullPath(LocalizationFolder);
      _executableRelativePath = version.Value;
    }

    public override void Install()
    {
      var assembly = Assembly.GetExecutingAssembly();

      var executable = assembly.GetResource(_resourceName);
      var archive = assembly.GetResource("ukr");

      var executablePath = GetFullPath(_executableRelativePath);

      // Backup existing executable
      LocalizationState.Backup.StoreFile(_executableRelativePath);

      // Create localized executable
      using (var file = File.Create(executablePath))
      {
        executable.CopyTo(file);
      }

      // Register modified file (executable)
      LocalizationState.Integrity.RegisterFile(_executableRelativePath, FileType.Existing);

      // Unzip localized data folder
      using (var zip = new ZipArchive(archive, ZipArchiveMode.Read))
      {
        zip.ExtractToDirectory(context.GameInstallationPath);
      }

      // Register added files
      var addedFiles = PathUtils.GetRelativeFilePaths(_localizationPath, context.GameInstallationPath);
      foreach (var addedFile in addedFiles)
      {
        LocalizationState.Integrity.RegisterFile(addedFile, FileType.New);
      }
    }

    public override void Uninstall()
    {
      base.Uninstall();
      Directory.Delete(_localizationPath);
    }
  }
}
