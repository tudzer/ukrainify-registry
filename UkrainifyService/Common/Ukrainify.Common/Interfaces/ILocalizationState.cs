﻿namespace Ukrainify
{
  public interface ILocalizationState
  {
    string Path { get; }
    ILocalizedFileRegistry LocalizedFileRegistry { get; }
    IBackupStorage Backup { get; }
    ILocalizationIntegrityVerifier Integrity { get; }

    void Clear();
  }
}
