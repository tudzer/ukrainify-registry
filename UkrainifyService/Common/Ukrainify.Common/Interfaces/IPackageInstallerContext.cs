﻿namespace Ukrainify
{
  public interface IPackageInstallerContext
  {
    string GameInstallationPath { get; }
  }
}
