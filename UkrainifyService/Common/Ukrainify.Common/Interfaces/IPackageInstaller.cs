﻿namespace Ukrainify
{
  public interface IPackageInstaller
  {
    ILocalizationState LocalizationState { get; }

    bool IsCompatible { get; }

    bool IsInstalled { get; }

    bool IsLocalizationModified { get; }

    void Install();

    void Uninstall();
  }
}