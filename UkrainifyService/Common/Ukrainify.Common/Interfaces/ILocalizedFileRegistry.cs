﻿using System.Collections.Generic;

namespace Ukrainify
{
  public interface ILocalizedFileRegistry
  {
    bool Empty { get; }
    IEnumerable<ILocalizedFileEntry> Entries { get; }
    IEnumerable<string> AddedFiles { get; }

    void AddEntry(string relativeFilePath, FileType type, IFileDigest digest);
  }
}
