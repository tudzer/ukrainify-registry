﻿namespace Ukrainify
{
  public interface ILocalizedFileEntry
  {
    string RelativePath { get; }

    FileType Type { get; }

    IFileDigest Digest { get; }
  }
}
