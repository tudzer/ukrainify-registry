﻿using System.Collections.Generic;

namespace Ukrainify
{
  public interface IBackupStorage
  {
    bool Empty { get; }
    void StoreFile(string relativeFilePath);
    void StoreFiles(IEnumerable<string> files);
    void Restore();
    void Restore(IEnumerable<string> relativeFilePaths);
  }
}
