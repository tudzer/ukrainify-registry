﻿namespace Ukrainify
{
  public interface IFileDigest
  {
    long LastModifiedTime { get; }

    long Size { get; }

    string Hash { get; }
  }
}
