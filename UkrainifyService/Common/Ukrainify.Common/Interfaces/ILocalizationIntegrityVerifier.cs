﻿using System.Collections.Generic;

namespace Ukrainify
{
  public interface ILocalizationIntegrityVerifier
  {
    bool IsModified { get; }
    IEnumerable<string> UnchangedFiles { get; }

    void RegisterFile(string relativeFilePath, FileType type);
  }
}
