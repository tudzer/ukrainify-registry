﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;

namespace Ukrainify
{
  public static class AssemblyExtensions
  {
    private static Stream ReadResource(Assembly assembly, string resourceName)
    {
      var identifier = $"Resources.{resourceName}";
      var fullName = assembly.GetManifestResourceNames().Single(name => name.EndsWith(identifier));
      var resource = assembly.GetManifestResourceStream(fullName);
      return resource ?? throw new Exception($"Resource {resourceName} not found.");
    }

    public static Stream GetResource(this Assembly assembly, string resourceName, bool decompress = true)
    {
      var resource = ReadResource(assembly, resourceName);
      if (!decompress) return resource;

      using (var inflate = new DeflateStream(resource, CompressionMode.Decompress))
      {
        var memoryStream = new MemoryStream();
        inflate.CopyTo(memoryStream);
        memoryStream.Seek(0, SeekOrigin.Begin);
        return memoryStream;
      }
    }
  }
}
