﻿using System.IO;

namespace Ukrainify.Unity
{
  public abstract class UnityGameInstallation
  {
    private const string ManagedDir = "Managed";

    public string GameDataPath { get; }

    public string ManagedPath { get; }

    protected UnityGameInstallation(string path, string gameDataDir)
    {
      GameDataPath = Path.Combine(path, gameDataDir);
      ManagedPath = Path.Combine(GameDataPath, ManagedDir);
    }
  }
}
