﻿using AssetsTools.NET;
using AssetsTools.NET.Extra;
using System.IO;
using System.Linq;

namespace Ukrainify.Unity
{
  public class FontPatcher : IAssetBinaryPatcher
  {
    private readonly AssetsFileContext _context;
    private readonly Stream _fontData;

    public FontPatcher(AssetsFileContext context, Stream fontData)
    {
      _context = context;
      _fontData = fontData;
    }

    public void Patch(string assetName)
    {
      var font = _context.GetAsset(AssetClassID.Font, assetName);
      if (font == null) throw new AssetNotFountException(_context.AssetsFileName, assetName);

      var fontTemplate = _context.GetAssetTemplate(font);
      var fontDataTemplate = fontTemplate.Children.FirstOrDefault(field => field.Name == "m_FontData");
      if (fontDataTemplate == null) throw new AssetNotFountException(_context.AssetsFileName, assetName);

      var fontDataArrayTemplate = fontDataTemplate.Children[0];
      fontDataArrayTemplate.ValueType = AssetValueType.ByteArray;
      fontDataArrayTemplate.Type = "TypelessData";

      var fontContent = _context.GetAssetContent(font, fontTemplate);

      using (var fontReader = new BinaryReader(_fontData))
      {
        var fontBytes = fontReader.ReadBytes((int)_fontData.Length);
        fontContent["m_FontData.Array"].AsByteArray = fontBytes;
      }

      font.SetNewData(fontContent);
    }
  }
}
