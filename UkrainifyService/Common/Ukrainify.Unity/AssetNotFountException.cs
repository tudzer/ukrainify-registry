﻿using System;

namespace Ukrainify.Unity
{
  public class AssetNotFountException : Exception
  {
    public AssetNotFountException()
    {
    }

    public AssetNotFountException(string assetsFileName, string assetName) : base($"Asset {assetName} not found in {assetsFileName}.") { }
  }
}
