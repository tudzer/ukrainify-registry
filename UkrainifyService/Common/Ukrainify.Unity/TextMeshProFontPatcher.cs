﻿using AssetsTools.NET.Extra;
using System.IO;

namespace Ukrainify.Unity
{
  public class TextMeshProFontPatcher : IAssetBinaryPatcher
  {
    private readonly AssetsFileContext _context;
    private readonly Stream _fontData;
    private readonly Stream _atlasData;

    public TextMeshProFontPatcher(AssetsFileContext context, Stream fontData, Stream atlasData)
    {
      _context = context;
      _fontData = fontData;
      _atlasData = atlasData;
    }

    public void Patch(string assetName)
    {
      var font = _context.GetAsset(AssetClassID.MonoBehaviour, assetName);
      if (font == null) throw new AssetNotFountException(_context.AssetsFileName, assetName);

      var fontContent = _context.GetAssetContent(font);
      var atlasPathId = fontContent["m_AtlasTextures.Array"][0]["m_PathID"].AsInt;
      var atlas = _context.GetAsset(atlasPathId);

      using (var fontReader = new BinaryReader(_fontData))
      {
        var fontBytes = fontReader.ReadBytes((int)_fontData.Length);
        font.SetNewData(fontBytes);
      }

      using (var atlasReader = new BinaryReader(_atlasData))
      {
        var atlasBytes = atlasReader.ReadBytes((int)_atlasData.Length);
        atlas.SetNewData(atlasBytes);
      }
    }
  }
}
