﻿using System.IO;

namespace Ukrainify.Unity
{
  internal interface IAssetBinaryPatcher
  {
    void Patch(string assetName);
  }
}
