﻿using AssetsTools.NET;
using AssetsTools.NET.Extra;
using System;
using System.IO;

namespace Ukrainify.Unity
{
  public class AssetsFileContext
  {
    private readonly AssetsManager _manager = new AssetsManager();
    private readonly AssetsFileInstance _instance;

    public string AssetsFileName => _instance.name;

    public AssetsFileContext(
      UnityGameInstallation gameInstallation,
      Stream classPackage,
      string assetsFileName,
      object templateGenerator = null
    )
    {
      _instance = _manager.LoadAssetsFile(Path.Combine(gameInstallation.GameDataPath, assetsFileName), false);

      if (classPackage == null) return;
      _manager.LoadClassPackage(classPackage);
      _manager.LoadClassDatabaseFromPackage(_instance.file.Metadata.UnityVersion);
      _manager.MonoTempGenerator = (IMonoBehaviourTemplateGenerator)templateGenerator ?? new MonoCecilTempGenerator(gameInstallation.ManagedPath);
    }

    public AssetFileInfo GetAsset(int pathId)
    {
      return _instance.file.GetAssetInfo(pathId);
    }

    public AssetFileInfo GetAsset(AssetClassID assetTypeId, string assetName)
    {
      var assets = _instance.file.GetAssetsOfType(assetTypeId);
      foreach (var asset in assets)
      {
        var content = GetAssetContent(asset);
        if (content == null) continue;
        var name = content["m_Name"].AsString;
        if (name == assetName)
        {
          return asset;
        }
      }
      return null;
    }

    public AssetTypeValueField GetAssetContent(AssetFileInfo asset)
    {
      try
      {
        return _manager.GetBaseField(_instance, asset);
      }
      catch { return null; }
    }

    public AssetTypeValueField GetAssetContent(AssetFileInfo asset, AssetTypeTemplateField template)
    {
      return template.MakeValue(_instance.file.Reader, asset.GetAbsoluteByteOffset(_instance.file));
    }

    public AssetTypeTemplateField GetAssetTemplate(AssetFileInfo asset)
    {
      return _manager.GetTemplateBaseField(_instance, asset);
    }

    public void SaveAndDispose()
    {
      var tempFilePath = $"{_instance.path}.tmp";

      using (var writer = new AssetsFileWriter(tempFilePath))
      {
        _instance.file.Write(writer);
        _instance.file.Close();
      }

      File.Delete(_instance.path);
      File.Move(tempFilePath, _instance.path);
    }
  }
}
