﻿namespace Ukrainify.Packing
{
  public class AsIsPackingStrategy : IPackingStrategy
  {
    public byte[] Pack(byte[] bytes, byte[] originalBytes = null)
    {
      return bytes;
    }

    public byte[] Unpack(byte[] bytes, byte[] originalBytes = null)
    {
      return bytes;
    }
  }
}
