﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using DeltaQ.BsDiff;
using DeltaQ.SuffixSorting.LibDivSufSort;

namespace Ukrainify.Packing
{
  public class AsDeltaPackingStrategy : IPackingStrategy
  {
    public byte[] Pack(byte[] bytes, byte[] originalBytes = null)
    {
      if (originalBytes == null)
      {
        throw new NotSupportedException("Cannot create delta without original bytes.");
      }

      var oldData = new Span<byte>(originalBytes);
      var newData = new Span<byte>(bytes);
      
      using (var patch = new MemoryStream())
      {
        Diff.Create(oldData, newData, patch, new LibDivSufSort());

        var patchBytes = patch.ToArray();
        return patchBytes;
      }
    }

    public byte[] Unpack(byte[] patchBytes, byte[] originalBytes = null)
    {
      if (originalBytes == null)
      {
        throw new NotSupportedException("Cannot apply delta without original bytes.");
      }

      var input = new ReadOnlyMemory<byte>(originalBytes);
      var diff = new ReadOnlyMemory<byte>(patchBytes);

      using (var output = new MemoryStream())
      {
        Patch.Apply(input, diff, output);

        var patchedBytes = output.ToArray();
        return patchedBytes;
      }
    }
  }
}
