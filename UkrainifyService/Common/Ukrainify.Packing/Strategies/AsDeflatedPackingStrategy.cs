﻿using System.IO;
using System.IO.Compression;

namespace Ukrainify.Packing
{
  public class AsDeflatedPackingStrategy : IPackingStrategy
  {
    public byte[] Pack(byte[] bytes, byte[] originalBytes = null)
    {
      using (var compressedData = new MemoryStream())
      {
        using (DeflateStream output = new DeflateStream(compressedData, CompressionMode.Compress))
        {
          output.Write(bytes, 0, bytes.Length);
        }

        var compressedBytes = compressedData.ToArray();
        return compressedBytes;
      }
    }

    public byte[] Unpack(byte[] bytes, byte[] originalBytes = null)
    {
      using (var compressedData = new MemoryStream(bytes))
      using (var decompressedData = new MemoryStream())
      using (var output = new DeflateStream(compressedData, CompressionMode.Decompress))
      {
        output.CopyTo(decompressedData);
        return decompressedData.ToArray();
      }
    }
  }
}
