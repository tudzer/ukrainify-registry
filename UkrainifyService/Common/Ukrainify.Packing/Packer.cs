﻿using System.Collections.Generic;

namespace Ukrainify.Packing
{
  public class Packer : IPacker
  {
    private readonly IDictionary<PackingStrategy, IPackingStrategy> _packingStrategies = new Dictionary<PackingStrategy, IPackingStrategy>
    {
      { PackingStrategy.AsIs, new AsIsPackingStrategy() },
      { PackingStrategy.AsDeflated, new AsDeflatedPackingStrategy() },
      { PackingStrategy.AsDelta, new AsDeltaPackingStrategy() },
    };

    public byte[] Pack(PackingStrategy strategy, byte[] bytes, byte[] originalBytes = null)
    {
      var packedBytes = _packingStrategies[strategy].Pack(bytes, originalBytes);
      return PackUtils.PrependPackingStrategyByte(packedBytes, strategy);
    }

    public byte[] Unpack(PackingStrategy strategy, byte[] bytes, byte[] originalBytes = null)
    {
      return _packingStrategies[strategy].Unpack(bytes, originalBytes);
    }
  }
}
