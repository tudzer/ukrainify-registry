﻿using System;

namespace Ukrainify.Packing
{
  internal static class PackUtils
  {
    public static byte[] PrependPackingStrategyByte(byte[] bytes, PackingStrategy packingStrategy)
    {
      byte[] result = new byte[bytes.Length + 1];

      result[0] = (byte)packingStrategy;
      Array.Copy(bytes, 0, result, 1, bytes.Length);

      return result;
    }
  }
}
