﻿namespace Ukrainify.Packing
{
  public enum PackingStrategy
  {
    AsIs,
    AsDeflated,
    AsDelta
  }
}
