﻿using System;

namespace Ukrainify.Packing
{
  public interface IPackingStrategy
  {
    byte[] Pack(byte[] bytes, byte[] originalBytes = null);
    byte[] Unpack(byte[] bytes, byte[] originalBytes = null);
  }
}
