﻿namespace Ukrainify.Packing
{
  public interface IPacker
  {
    byte[] Pack(PackingStrategy strategy, byte[] bytes, byte[] originalBytes = null);
    byte[] Unpack(PackingStrategy strategy, byte[] bytes, byte[] originalBytes = null);
  }
}
