﻿namespace Ukrainify.PackageInstallers
{
  public class ArchivePackageInstallerContext : PackageInstallerContext
  {
    public string ArchivePath { get; }

    public ArchivePackageInstallerContext(string gameInstallationPath, string archivePath)
      : base(gameInstallationPath)
    {
      ArchivePath = archivePath;
    }
  }
}
