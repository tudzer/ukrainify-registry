﻿namespace Ukrainify.PackageInstallers
{
  public class CustomPackageInstallerContext : PackageInstallerContext
  {
    public CustomPackageInstallerContext(string gameInstallationPath)
      : base(gameInstallationPath)
    { }
  }
}
