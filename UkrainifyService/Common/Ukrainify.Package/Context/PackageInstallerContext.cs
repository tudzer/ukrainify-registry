﻿namespace Ukrainify.PackageInstallers
{
  public abstract class PackageInstallerContext : IPackageInstallerContext
  {
    public string GameInstallationPath { get; }

    protected PackageInstallerContext(string gameInstallationPath)
    {
      GameInstallationPath = gameInstallationPath;
    }
  }
}
