﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Ukrainify.PackageInstallers
{
  public static class PathUtils
  {
    public static IEnumerable<string> GetRelativeFilePaths(string sourcePath, string basePath)
    {
      var allFiles = Directory.EnumerateFiles(sourcePath, "*", SearchOption.AllDirectories);
      foreach (var file in allFiles)
      {
        yield return GetRelativePath(file, basePath);
      }
    }

    public static string GetRelativePath(string fullPath, string basePath)
    {
      Uri fileUri = new Uri(fullPath);
      Uri rootUri = new Uri(basePath);

      if (!basePath.EndsWith(Path.DirectorySeparatorChar.ToString()))
      {
        rootUri = new Uri(basePath + Path.DirectorySeparatorChar);
      }

      Uri relativeUri = rootUri.MakeRelativeUri(fileUri);
      string relativePath = Uri.UnescapeDataString(relativeUri.ToString()).Replace('/', Path.DirectorySeparatorChar);

      return relativePath;
    }
  }
}
