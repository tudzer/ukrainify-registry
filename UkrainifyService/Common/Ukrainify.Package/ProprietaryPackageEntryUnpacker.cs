﻿using System;
using System.IO;
using Ukrainify.Packing;

namespace Ukrainify.PackageInstallers
{
  internal class ProprietaryPackageEntryUnpacker
  {
    private readonly IPacker _packer;

    public ProprietaryPackageEntryUnpacker(IPacker packer)
    {
      _packer = packer;
    }

    public void Unpack(Stream entry, string targetFilePath)
    {
      var (packingStrategy, packedData) = ReadFileWithPackingStrategy(entry);
      byte[] originalBytes = null;
      if (packingStrategy == PackingStrategy.AsDelta)
      {
        if (!File.Exists(targetFilePath))
        {
          throw new InvalidOperationException("Cannot unpack delta entry without original file.");
        }
        originalBytes = File.ReadAllBytes(targetFilePath);
      }
      var unpackedData = _packer.Unpack(packingStrategy, packedData, originalBytes);
      File.WriteAllBytes(targetFilePath, unpackedData);
    }

    private static (PackingStrategy, byte[]) ReadFileWithPackingStrategy(Stream entry)
    {
      var packingStrategyByte = entry.ReadByte();
      if (packingStrategyByte == -1)
      {
        throw new ArgumentException("Entry is empty");
      }
      if (!Enum.IsDefined(typeof(PackingStrategy), packingStrategyByte))
      {
        throw new ArgumentException("Entry doesn't contain a valid packing strategy byte.");
      }

      var packingStrategy = (PackingStrategy)packingStrategyByte;

      using (var entryData = new MemoryStream())
      {
        entry.CopyTo(entryData);
        return (packingStrategy, entryData.ToArray());
      }
    }
  }
}
