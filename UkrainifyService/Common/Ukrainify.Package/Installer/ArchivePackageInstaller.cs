﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Ukrainify.PackageInstallers
{
  public class ArchivePackageInstaller : PackageInstaller<ArchivePackageInstallerContext>
  {
    private static readonly string DirectorySeparator = Path.DirectorySeparatorChar.ToString();
    private static readonly string AltDirectorySeparator = Path.AltDirectorySeparatorChar.ToString();

    public ArchivePackageInstaller(ArchivePackageInstallerContext context, ILocalizationState localizationState) : base(context, localizationState)
    { }

    public override void Install()
    {
      using (var package = OpenPackage())
      {
        var newFiles = new List<(ZipArchiveEntry Entry, string FullPath)>();
        var existingFiles = new List<(ZipArchiveEntry Entry, string FullPath)>();

        // Split new/existing files and resolve full paths
        foreach (var entry in package.Entries)
        {
          if (IsDirectoryEntry(entry)) continue;

          var fullPath = GetFullPath(entry.FullName);
          var targetList = File.Exists(fullPath) ? existingFiles : newFiles;
          targetList.Add((entry, fullPath));
        }

        // Backup existing files
        LocalizationState.Backup.StoreFiles(existingFiles.Select(e => e.Entry.FullName));

        // Replace existing files & register them for integrity verification
        foreach (var (entry, fullPath) in existingFiles)
        {
          InstallEntry(entry, fullPath);
          LocalizationState.Integrity.RegisterFile(entry.FullName, FileType.Existing);
        }

        // Add new files & register them for integrity verification
        foreach (var (entry, fullPath) in newFiles)
        {
          InstallEntry(entry, fullPath);
          LocalizationState.Integrity.RegisterFile(entry.FullName, FileType.New);
        }
      }
    }

    protected virtual void InstallEntry(ZipArchiveEntry entry, string targetFilePath)
    {
      entry.ExtractToFile(targetFilePath, true);
    }

    private ZipArchive OpenPackage()
    {
      var stream = File.OpenRead(context.ArchivePath);
      return new ZipArchive(stream, ZipArchiveMode.Read);
    }

    private static bool IsDirectoryEntry(ZipArchiveEntry entry)
    {
      return entry.FullName.EndsWith(DirectorySeparator) || entry.FullName.EndsWith(AltDirectorySeparator);
    }
  }
}
