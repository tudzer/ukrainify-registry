﻿using System.IO.Compression;
using Ukrainify.Packing;

namespace Ukrainify.PackageInstallers
{
  public class ProprietaryPackageInstaller : ArchivePackageInstaller
  {
    private readonly ProprietaryPackageEntryUnpacker _entryUnpacker;

    public ProprietaryPackageInstaller(ArchivePackageInstallerContext context, ILocalizationState localizationState) : base(context, localizationState)
    {
      _entryUnpacker = new ProprietaryPackageEntryUnpacker(new Packer());
    }

    protected override void InstallEntry(ZipArchiveEntry entry, string targetFilePath)
    {
      using (var entryStream = entry.Open())
      {
        _entryUnpacker.Unpack(entryStream, targetFilePath);
      }
    }
  }
}
