﻿using System.IO;

namespace Ukrainify.PackageInstallers
{
  public abstract class PackageInstaller<TContext> : IPackageInstaller where TContext : IPackageInstallerContext
  {
    public ILocalizationState LocalizationState { get; }

    public virtual bool IsCompatible => true;

    public virtual bool IsInstalled => !LocalizationState.LocalizedFileRegistry.Empty && !LocalizationState.Integrity.IsModified;

    public virtual bool IsLocalizationModified => LocalizationState.Integrity.IsModified;

    protected readonly TContext context;

    protected PackageInstaller(TContext context, ILocalizationState localizationState)
    {
      this.context = context;
      LocalizationState = localizationState;
    }

    public abstract void Install();

    public virtual void Uninstall()
    {
      if (!LocalizationState.Backup.Empty)
      {
        var filesToRestore = LocalizationState.Integrity.UnchangedFiles;
        LocalizationState.Backup.Restore(filesToRestore);
      }
      
      var filesToRemove = LocalizationState.LocalizedFileRegistry.AddedFiles;
      foreach (var relativeFilePath in filesToRemove)
      {
        var filePath = GetFullPath(relativeFilePath);
        File.Delete(filePath);
      }
    }

    protected string GetFullPath(string relativePath)
    {
      return Path.Combine(context.GameInstallationPath, relativePath);
    }
  }
}
