﻿using System;
using System.IO;
using System.Text;
using ValveKeyValue.Abstraction;
using ValveKeyValue.KeyValues1;

namespace ValveKeyValue.Serialization.KeyValues1
{
  sealed class KV1BinarySerializer : IVisitationListener, IDisposable
  {
    public KV1BinarySerializer(Stream stream)
    {
      Require.NotNull(stream, nameof(stream));

      writer = new BinaryWriter(stream, Encoding.UTF8, leaveOpen: true);
    }

    readonly BinaryWriter writer;
    int objectDepth;

    public void Dispose()
    {
      writer.Dispose();
    }

    public void OnObjectStart(string name)
    {
      objectDepth++;
      Write(KV1BinaryNodeType.ChildObject);
      WriteNullTerminatedBytes(Encoding.UTF8.GetBytes(name));
    }

    public void OnObjectEnd()
    {
      Write(KV1BinaryNodeType.End);

      objectDepth--;
      if (objectDepth == 0)
      {
        Write(KV1BinaryNodeType.End);
      }
    }

    public void OnKeyValuePair(string name, KVValue value)
    {
      Write(GetNodeType(value.ValueType));
      WriteNullTerminatedBytes(Encoding.UTF8.GetBytes(name));

      switch (value.ValueType)
      {
        case KVValueType.FloatingPoint:
          writer.Write((float)value);
          break;

        case KVValueType.Int32:
        case KVValueType.Pointer:
          writer.Write((int)value);
          break;

        case KVValueType.String:
          WriteNullTerminatedBytes(Encoding.UTF8.GetBytes((string)value));
          break;

        case KVValueType.UInt64:
          writer.Write((ulong)value);
          break;

        case KVValueType.Int64:
          writer.Write((long)value);
          break;

        default:
          throw new ArgumentOutOfRangeException(nameof(value.ValueType), value.ValueType, "Unhandled value type.");
      }
    }

    void Write(KV1BinaryNodeType nodeType)
    {
      writer.Write((byte)nodeType);
    }

    void WriteNullTerminatedBytes(byte[] value)
    {
      writer.Write(value);
      writer.Write((byte)0);
    }

    static KV1BinaryNodeType GetNodeType(KVValueType type)
    {
      KV1BinaryNodeType result;

      switch (type)
      {
        case KVValueType.FloatingPoint:
          result = KV1BinaryNodeType.Float32;
          break;
        case KVValueType.Int32:
          result = KV1BinaryNodeType.Int32;
          break;
        case KVValueType.Pointer:
          result = KV1BinaryNodeType.Pointer;
          break;
        case KVValueType.String:
          result = KV1BinaryNodeType.String;
          break;
        case KVValueType.UInt64:
          result = KV1BinaryNodeType.UInt64;
          break;
        case KVValueType.Int64:
          result = KV1BinaryNodeType.Int64;
          break;
        default:
          throw new ArgumentOutOfRangeException(nameof(type), type, "Unhandled value type.");
      }

      return result;
    }
  }
}
