﻿using Gamelib.Core.Util;
using GameLib.Core;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;

namespace GameLib.Plugin.Gog
{
  public class GogLauncher : ILauncher
  {
    public GogLauncher(LauncherOptions launcherOptions = null)
    {
      LauncherOptions = launcherOptions ?? new LauncherOptions();
    }

    #region Interface implementations
    public LauncherOptions LauncherOptions { get; }

    public Guid Id => GetType().GUID;

    public string Name => "GOG Galaxy";

    public bool IsInstalled { get; private set; }

    public bool IsRunning => ProcessUtil.IsProcessRunning(Executable);

    public string InstallDir { get; private set; } = string.Empty;

    public string Executable { get; private set; } = string.Empty;

    public Icon ExecutableIcon => PathUtil.GetFileIcon(Executable);

    public IEnumerable<IGame> Games { get; private set; } = Enumerable.Empty<IGame>();

    public void Refresh(CancellationToken cancellationToken = default)
    {
      Executable = string.Empty;
      InstallDir = string.Empty;
      IsInstalled = false;
      Games = Enumerable.Empty<IGame>();

      Executable = GetExecutable() ?? string.Empty;
      if (!string.IsNullOrEmpty(Executable))
      {
        InstallDir = Path.GetDirectoryName(Executable) ?? string.Empty;
        IsInstalled = File.Exists(Executable);
        Games = GogGameFactory.GetGames(this, cancellationToken);
      }
    }

    public bool Start() => IsRunning || ProcessUtil.StartProcess(Executable);

    public void Stop()
    {
      if (IsRunning)
      {
        Process.Start(Executable, "/command=shutdown");
      }
    }
    #endregion

    #region Private methods
    private static string GetExecutable()
    {
      string executablePath = RegistryUtil.GetShellCommand("goggalaxy");

      if (executablePath == null) RegistryUtil.GetValue(RegistryHive.LocalMachine, @"SOFTWARE\GOG.com\GalaxyClient\paths", "client");

      executablePath = PathUtil.Sanitize(executablePath);

      if (!string.IsNullOrEmpty(executablePath) && !PathUtil.IsExecutable(executablePath))
      {
        executablePath = Path.Combine(executablePath, RegistryUtil.GetValue(RegistryHive.LocalMachine, @"SOFTWARE\GOG.com\GalaxyClient", "clientExecutable") ?? string.Empty);
      }

      if (!PathUtil.IsExecutable(executablePath))
      {
        executablePath = null;
      }

      return executablePath;
    }
    #endregion

  }
}