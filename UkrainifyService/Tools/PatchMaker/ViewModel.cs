﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Ukrainify.PackageCreationTool
{
  public class ViewModel : INotifyPropertyChanged
  {
    private readonly IFileRecordManager _fileRecordManager;
    private string _originalFilesPath = Properties.Settings.Default.LastOriginalFilesPath;
    private string _modifiedFilesPath = Properties.Settings.Default.LastModifiedFilesPath;
    private double _progressValue = 0;
    private bool _processing = false;

    public string OriginalFilesPath
    {
      get => _originalFilesPath;
      set
      {
        _originalFilesPath = value;
        OnPropertyChanged(nameof(OriginalFilesPath));
      }
    }

    public string ModifiedFilesPath
    {
      get => _modifiedFilesPath;
      set
      {
        _modifiedFilesPath = value;
        OnPropertyChanged(nameof(ModifiedFilesPath));
      }
    }

    public double ProgressValue
    {
      get => _progressValue;
      set
      {
        _progressValue = value;
        OnPropertyChanged(nameof(ProgressValue));
      }
    }

    public bool Processing
    {
      get => _processing;
      set
      {
        _processing = value;
        OnPropertyChanged(nameof(CanCreatePackage));
        OnPropertyChanged(nameof(Processing));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public IEnumerable<FileRecord> FileRecords => _fileRecordManager.FileRecords;

    public string FilesCountDisplayValue => string.Format((string)Application.Current.Resources["filesCount"], FileRecords.Count());

    public bool CanCreatePackage => !Processing && FileRecords.Any();

    public ViewModel(IFileRecordManager fileRecordManager)
    {
      _fileRecordManager = fileRecordManager;
      _fileRecordManager.FileRecordsUpdated += FileRecordsUpdated;
      if (PathHelper.Exists(_originalFilesPath) && PathHelper.Exists(_modifiedFilesPath))
      {
        _fileRecordManager.LoadFileRecords(_originalFilesPath, _modifiedFilesPath);
      }
    }

    private void FileRecordsUpdated(object sender, EventArgs e)
    {
      OnPropertyChanged(nameof(FileRecords));
      OnPropertyChanged(nameof(FilesCountDisplayValue));
      OnPropertyChanged(nameof(CanCreatePackage));
    }

    protected virtual void OnPropertyChanged(string propertyName)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

      if (propertyName == nameof(OriginalFilesPath) || propertyName == nameof(ModifiedFilesPath))
      {
        _fileRecordManager.LoadFileRecords(_originalFilesPath, _modifiedFilesPath);
        if (propertyName == nameof(OriginalFilesPath))
        {
          Properties.Settings.Default.LastOriginalFilesPath = OriginalFilesPath;
        }
        else
        {
          Properties.Settings.Default.LastModifiedFilesPath = ModifiedFilesPath;
        }
        Properties.Settings.Default.Save();
      }
    }
  }
}