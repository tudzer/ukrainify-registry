﻿namespace Ukrainify.PackageCreationTool
{
  internal interface IFileHeader
  {
    int Length { get; }

    bool IsPresent(byte[] fileBytes);
  }
}
