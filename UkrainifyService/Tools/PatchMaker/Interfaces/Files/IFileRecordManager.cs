﻿using System;
using System.Collections.Generic;

namespace Ukrainify.PackageCreationTool
{
  public interface IFileRecordManager
  {
    IEnumerable<FileRecord> FileRecords { get; }

    event EventHandler FileRecordsUpdated;

    void LoadFileRecords(string modifiedFilesPath, string originalFilesPath);
    void RemoveFileRecords(IEnumerable<FileRecord> records);
  }
}