﻿using System.Collections.Generic;

namespace Ukrainify.PackageCreationTool
{
  public interface IFileRecordFactory
  {
    IEnumerable<FileRecord> CreateRecords(string originalFilesPath, string modifiedFilesPath);
  }
}