﻿using Ukrainify.Packing;

namespace Ukrainify.PackageCreationTool
{
  internal interface IFilePackingStrategyEvaluator
  {
    PackingStrategy Evaluate(string filePath, FileType fileType);
  }
}
