﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ukrainify.PackageCreationTool.Packing
{
  public interface IPackageCreator
  {
    Task<byte[]> CreatePackageAsync(IEnumerable<FileRecord> records, IProgress<double> progress);
  }
}