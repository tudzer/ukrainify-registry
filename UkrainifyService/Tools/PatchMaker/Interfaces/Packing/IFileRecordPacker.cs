﻿namespace Ukrainify.PackageCreationTool.Packing
{
  internal interface IFileRecordPacker
  {
    byte[] Pack(FileRecord fileRecord);
  }
}