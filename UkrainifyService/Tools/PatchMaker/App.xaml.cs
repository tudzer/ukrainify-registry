﻿using Autofac;
using System.Windows;

namespace Ukrainify.PackageCreationTool
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    private void Application_Startup(object sender, StartupEventArgs e)
    {
      var container = DependencyInjection.CreateContainer();
      container.Resolve<MainWindow>().Show();
    }
  }
}
