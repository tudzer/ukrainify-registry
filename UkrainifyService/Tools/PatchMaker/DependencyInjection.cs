﻿using Autofac;
using Ukrainify.Packing;
using Ukrainify.PackageCreationTool.Packing;

namespace Ukrainify.PackageCreationTool
{
  internal static class DependencyInjection
  {
    public static IContainer CreateContainer()
    {
      var builder = new ContainerBuilder();

      builder.RegisterType<Packer>().As<IPacker>().SingleInstance();
      builder.RegisterType<FileRecordPacker>().As<IFileRecordPacker>().SingleInstance();
      builder.RegisterType<PackageCreator>().As<IPackageCreator>().SingleInstance();
      builder.RegisterType<FilePackingStrategyEvaluator>().As<IFilePackingStrategyEvaluator>().SingleInstance();
      builder.RegisterType<FileRecordFactory>().As<IFileRecordFactory>().SingleInstance();
      builder.RegisterType<FileRecordManager>().As<IFileRecordManager>().SingleInstance();
      builder.RegisterType<ViewModel>().AsSelf().SingleInstance();
      builder.RegisterType<MainWindow>().AsSelf().SingleInstance();

      var container = builder.Build();
      return container;
    }
  }
}
