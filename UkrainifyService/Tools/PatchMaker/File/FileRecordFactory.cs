﻿using System.Collections.Generic;
using System.Linq;

namespace Ukrainify.PackageCreationTool
{
  internal class FileRecordFactory : IFileRecordFactory
  {
    private readonly IFilePackingStrategyEvaluator _packingStrategyEvaluator;

    public FileRecordFactory(IFilePackingStrategyEvaluator packingStrategyEvaluator)
    {
      _packingStrategyEvaluator = packingStrategyEvaluator;
    }

    public IEnumerable<FileRecord> CreateRecords(string originalFilesPath, string modifiedFilesPath)
    {
      var records = PathHelper.GetAllFiles(modifiedFilesPath)
        .Select(filePath =>
        {
          var relativePath = PathHelper.GetRelativePath(filePath, modifiedFilesPath);
          var originalFilePath = PathHelper.GetFullPath(relativePath, originalFilesPath);
          var extension = PathHelper.GetExtension(relativePath);
          var type = PathHelper.Exists(originalFilePath) ? FileType.Existing : FileType.New;
          var packingStrategy = _packingStrategyEvaluator.Evaluate(filePath, type);

          return new FileRecord(filePath, relativePath, originalFilePath, extension, type, packingStrategy);
        });

      return records;
    }
  }
}
