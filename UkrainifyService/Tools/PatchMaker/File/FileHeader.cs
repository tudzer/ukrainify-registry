﻿using System.Collections.Generic;
using System.Linq;

namespace Ukrainify.PackageCreationTool
{
  internal class FileHeader : IFileHeader
  {
    private readonly IReadOnlyList<byte?> _pattern;

    public int Length { get; }

    public FileHeader(string pattern)
    {
      _pattern = pattern.Select(c => c == '*' ? null : (byte?)c).ToList();
      Length = pattern.Length;
    }

    public bool IsPresent(byte[] fileBytes)
    {
      for (var i = 0; i < Length; i++)
      {
        if (_pattern[i] == null) continue;
        if (fileBytes[i] != _pattern[i]) return false;
      }
      return true;
    }
  }
}
