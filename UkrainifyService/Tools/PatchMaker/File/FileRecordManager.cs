﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Ukrainify.PackageCreationTool
{
  public class FileRecordManager : IFileRecordManager
  {
    private readonly ObservableCollection<FileRecord> _fileRecords = new ObservableCollection<FileRecord>();
    private readonly IFileRecordFactory _fileRecordFactory;

    public IEnumerable<FileRecord> FileRecords => _fileRecords;

    public event EventHandler FileRecordsUpdated;

    public FileRecordManager(IFileRecordFactory fileRecordFactory)
    {
      _fileRecordFactory = fileRecordFactory;
    }

    protected virtual void OnFilesUpdated()
    {
      FileRecordsUpdated?.Invoke(this, EventArgs.Empty);
    }

    public void LoadFileRecords(string modifiedFilesPath, string originalFilesPath)
    {
      _fileRecords.Clear();
      var newRecords = _fileRecordFactory.CreateRecords(modifiedFilesPath, originalFilesPath);
      foreach (var record in newRecords)
      {
        _fileRecords.Add(record);
      }
      OnFilesUpdated();
    }

    public void RemoveFileRecords(IEnumerable<FileRecord> records)
    {
      foreach (var record in records)
      {
        _fileRecords.Remove(record);
      }
      OnFilesUpdated();
    }
  }
}
