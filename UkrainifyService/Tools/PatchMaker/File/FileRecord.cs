﻿using System.ComponentModel;
using Ukrainify.Packing;

namespace Ukrainify.PackageCreationTool
{
  public class FileRecord : INotifyPropertyChanged
  {
    private PackingStrategy _packingStrategy;

    public string FilePath { get; }

    public string RelativePath { get; }

    public string OriginalFilePath { get; }

    public string Extension { get; }

    public FileType Type { get; }

    public PackingStrategy PackingStrategy
    {
      get => _packingStrategy;
      set
      {
        _packingStrategy = value;
        OnPropertyChanged(nameof(PackingStrategy));
      }
    }

    public FileRecord(string filePath, string originalFilePath, string modifiedFilePath, string extension, FileType type, PackingStrategy packingStrategy)
    {
      FilePath = filePath;
      RelativePath = originalFilePath;
      OriginalFilePath = modifiedFilePath;
      Extension = extension;
      Type = type;
      _packingStrategy = packingStrategy;
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged(string propertyName)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
