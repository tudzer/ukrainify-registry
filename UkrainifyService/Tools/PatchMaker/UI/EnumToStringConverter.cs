﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Ukrainify.PackageCreationTool.UI
{
  public class EnumToDisplayValueConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value is Enum ? Application.Current.Resources[$"{value.GetType().Name}.{value}"] : string.Empty;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}