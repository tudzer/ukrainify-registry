﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Ukrainify.Packing;

namespace Ukrainify.PackageCreationTool.UI
{
  internal class FileRecordToPackingStrategyConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (!(value is FileRecord record)) return null;

      var packingStrategies = Enum.GetValues(typeof(PackingStrategy)).Cast<PackingStrategy>();

      return record.Type == FileType.New
        ? packingStrategies.Where(strategy => strategy != PackingStrategy.AsDelta)
        : packingStrategies;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
