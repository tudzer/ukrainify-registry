﻿using System.IO;
using Ukrainify.Packing;

namespace Ukrainify.PackageCreationTool.Packing
{
  internal class FileRecordPacker : IFileRecordPacker
  {
    private readonly IPacker _packer;

    public FileRecordPacker(IPacker packer)
    {
      _packer = packer;
    }

    public byte[] Pack(FileRecord fileRecord)
    {
      var bytes = File.ReadAllBytes(fileRecord.FilePath);
      byte[] originalBytes = null;

      if (fileRecord.PackingStrategy == PackingStrategy.AsDelta)
      {
        originalBytes = File.ReadAllBytes(fileRecord.OriginalFilePath);
      }

      return _packer.Pack(fileRecord.PackingStrategy, bytes, originalBytes);
    }
  }
}
