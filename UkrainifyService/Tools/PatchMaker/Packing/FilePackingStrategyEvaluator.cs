﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ukrainify.Packing;

namespace Ukrainify.PackageCreationTool.Packing
{
  internal class FilePackingStrategyEvaluator : IFilePackingStrategyEvaluator
  {
    private readonly IEnumerable<string> _nonDeltaExtensions;
    private readonly IEnumerable<IFileHeader> _nonDeltaHeaders;
    private readonly int _longestHeaderLength;

    public FilePackingStrategyEvaluator()
    {
      _nonDeltaExtensions = Properties.Settings.Default.NonDeltaExtensions.Split(';');
      _nonDeltaHeaders = Properties.Settings.Default.NonDeltaHeaders.Split(';').Select(x => new FileHeader(x));
      _longestHeaderLength = _nonDeltaHeaders.Select(x => x.Length).Max();
    }

    public PackingStrategy Evaluate(string filePath, FileType fileType)
    {
      if (fileType == FileType.New) return PackingStrategy.AsDeflated;

      var fileExtension = PathHelper.GetExtension(filePath);
      var isNonDeltaFileExtension = _nonDeltaExtensions.Any(extension => string.Equals(extension, fileExtension, System.StringComparison.OrdinalIgnoreCase));
      if (isNonDeltaFileExtension) return PackingStrategy.AsDeflated;

      var fileBytes = ReadBytes(filePath, _longestHeaderLength);
      var isNonDeltaHeader = _nonDeltaHeaders.Any(header => header.IsPresent(fileBytes));
      return isNonDeltaHeader ? PackingStrategy.AsIs : PackingStrategy.AsDelta;
    }

    private static byte[] ReadBytes(string filePath, int length)
    {
      var buffer = new byte[length];

      using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
      {
        fileStream.Read(buffer, 0, length);
      }

      return buffer;
    }
  }
}
