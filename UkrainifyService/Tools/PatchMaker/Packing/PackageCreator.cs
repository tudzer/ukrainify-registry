﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ukrainify.PackageCreationTool.Packing
{
  internal class PackageCreator : IPackageCreator
  {
    private readonly IFileRecordPacker _fileRecordPacker;

    public PackageCreator(IFileRecordPacker fileRecordPacker)
    {
      _fileRecordPacker = fileRecordPacker;
    }

    public Task<byte[]> CreatePackageAsync(IEnumerable<FileRecord> records, IProgress<double> progress)
    {
      IList<FileRecord> recordsToProcess = records.ToList();

      var processedCount = 0;
      var totalCount = recordsToProcess.Count();

      var packedFiles = new ConcurrentDictionary<FileRecord, byte[]>();
      var maxThreads = Properties.Settings.Default.MaxDeltaCalculationThreads;

      while (recordsToProcess.Any())
      {
        foreach (var fileRecord in recordsToProcess)
        {
          try
          {
            var bytes = _fileRecordPacker.Pack(fileRecord);
            packedFiles.TryAdd(fileRecord, bytes);

            var updatedCount = Interlocked.Increment(ref processedCount);
            progress.Report((double)updatedCount / totalCount * 100);
          }
          catch (OutOfMemoryException)
          { }
        }
        recordsToProcess = recordsToProcess.Where(fileInfo => !packedFiles.Keys.Contains(fileInfo)).ToList();
        if (maxThreads > 1) maxThreads--;
      }

      var package = PackFiles(packedFiles.Select(pair => (pair.Key.RelativePath, pair.Value)));

      return Task.FromResult(package);
    }

    private static byte[] PackFiles(IEnumerable<(string, byte[])> files)
    {
      using (var package = new MemoryStream())
      {
        using (var archive = new ZipArchive(package, ZipArchiveMode.Create, true))
        {
          foreach (var (relativePath, bytes) in files)
          {
            var entry = archive.CreateEntry(relativePath, CompressionLevel.NoCompression);

            using (var entryStream = entry.Open())
            {
              entryStream.Write(bytes, 0, bytes.Length);
            }
          }
        }

        return package.ToArray();
      }
    }
  }
}
