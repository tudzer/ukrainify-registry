﻿using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Linq;
using Ookii.Dialogs.Wpf;

namespace Ukrainify.PackageCreationTool.Controls
{
  /// <summary>
  /// Interaction logic for UserControl1.xaml
  /// </summary>
  public partial class FolderSelector : UserControl
  {
    public string Caption
    {
      get => (string)GetValue(CaptionProperty);
      set => SetValue(CaptionProperty, value);
    }

    public string SelectedPath
    {
      get => (string)GetValue(SelectedPathProperty);
      set => SetValue(SelectedPathProperty, value);
    }

    public FolderSelector()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty SelectedPathProperty = DependencyProperty.Register(
      "SelectedPath",
      typeof(string),
      typeof(FolderSelector)
    );

    public static readonly DependencyProperty CaptionProperty =
      DependencyProperty.Register(
        "Caption",
        typeof(string),
        typeof(FolderSelector)
      );

    private void Input_PreviewDragOver(object sender, DragEventArgs e)
    {
      e.Effects = GetSingleDirectory(e) != null ? DragDropEffects.Copy : DragDropEffects.None;
      e.Handled = true;
    }

    private void Input_PreviewDrop(object sender, DragEventArgs e)
    {
      SelectedPath = GetSingleDirectory(e);
    }

    private string GetSingleDirectory(DragEventArgs e)
    {
      if (e.Data.GetDataPresent(DataFormats.FileDrop))
      {
        var files = e.Data.GetData(DataFormats.FileDrop) as string[];
        var file = files?.FirstOrDefault();

        if (file != null && Directory.Exists(file))
        {
          return file;
        }
      }

      return null;
    }

    private void Specify_Click(object sender, RoutedEventArgs e)
    {
      var folderDialog = new VistaFolderBrowserDialog
      {
        SelectedPath = SelectedPath,
      };
      var result = folderDialog.ShowDialog();
      if (result.HasValue && result.Value)
      {
        SelectedPath = folderDialog.SelectedPath;
      }
    }
  }
}
