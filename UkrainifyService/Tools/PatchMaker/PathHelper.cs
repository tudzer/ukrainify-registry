﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Ukrainify.PackageCreationTool
{
  internal class PathHelper
  {
    public static List<string> GetAllFiles(string rootPath)
    {
      if (!Directory.Exists(rootPath)) return new List<string>();

      var allFiles = new List<string>();
      GetAllFilesRecursive(rootPath, rootPath, allFiles);
      return allFiles;
    }

    private static void GetAllFilesRecursive(string rootPath, string currentPath, List<string> files)
    {
      DirectoryInfo directory = new DirectoryInfo(currentPath);
      foreach (var file in directory.GetFiles())
      {
        files.Add(file.FullName);
      }
      Debug.WriteLine(rootPath);
      Debug.WriteLine(currentPath);
      foreach (var subDir in directory.GetDirectories())
      {
        GetAllFilesRecursive(rootPath, subDir.FullName, files);
      }
    }

    public static string GetRelativePath(string fullPath, string rootPath)
    {
      Uri fileUri = new Uri(fullPath);
      Uri rootUri = new Uri(rootPath);

      if (!rootPath.EndsWith(Path.DirectorySeparatorChar.ToString()))
      {
        rootUri = new Uri(rootPath + Path.DirectorySeparatorChar);
      }

      Uri relativeUri = rootUri.MakeRelativeUri(fileUri);
      string relativePath = Uri.UnescapeDataString(relativeUri.ToString()).Replace('/', Path.DirectorySeparatorChar);

      return relativePath;
    }

    public static string GetFullPath(string relativePath, string rootPath)
    {
      return Path.Combine(rootPath, relativePath);
    }

    public static string GetExtension(string filePath)
    {
      var extension = Path.GetExtension(filePath);
      return string.IsNullOrEmpty(extension) ? extension : extension.Substring(1);
    }

    public static bool Exists(string path)
    {
      if (string.IsNullOrEmpty(path)) return false;
      return File.Exists(path) || Directory.Exists(path);
    }

    public static void WriteToFile(string filePath, byte[] bytes)
    {
      File.WriteAllBytes(filePath, bytes);
    }
  }
}
