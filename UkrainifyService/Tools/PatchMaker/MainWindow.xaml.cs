﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Ookii.Dialogs.Wpf;
using Ukrainify.Packing;
using Ukrainify.PackageCreationTool.Packing;

namespace Ukrainify.PackageCreationTool
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private readonly ViewModel _viewModel;
    private readonly IPackageCreator _packageCreator;
    private readonly IFileRecordManager _fileRecordManager;

    public MainWindow(ViewModel viewModel, IPackageCreator packageCreator, IFileRecordManager fileRecordManager)
    {
      InitializeComponent();
      _viewModel = viewModel;
      _packageCreator = packageCreator;
      _fileRecordManager = fileRecordManager;
      DataContext = viewModel;
    }

    private void Start_Click(object sender, RoutedEventArgs e)
    {
      var progress = new Progress<double>(value =>
      {
        _viewModel.ProgressValue = value;
      });


      Task.Run(async () =>
      {
        var package = await _packageCreator.CreatePackageAsync(_viewModel.FileRecords, progress);
        var dialog = new VistaSaveFileDialog
        {
          FileName = GetPackageName(),
          DefaultExt = Properties.Resources.DefaultPackageExtension,
          Filter = Properties.Resources.DialogFileFilter,
          InitialDirectory = Properties.Settings.Default.LastFileSaveDirectory
        };

        var result = dialog.ShowDialog();
        if (result == true)
        {
          PathHelper.WriteToFile(dialog.FileName, package);
        }
      });
    }

    private static T FindParent<T>(DependencyObject child) where T : DependencyObject
    {
      DependencyObject parentObj = VisualTreeHelper.GetParent(child);
      if (parentObj == null) return null;

      if (parentObj is T parent)
      {
        return parent;
      }
      else
      {
        return FindParent<T>(parentObj);
      }
    }

    private void OnPackingStrategyChange(object sender, SelectionChangedEventArgs e)
    {
      if (!(sender is ComboBox comboBox) || comboBox.SelectedItem == null) return;

      var record = (FileRecord)comboBox.DataContext;
      var listViewItem = FindParent<ListViewItem>(comboBox);
      if (listViewItem == null || !listViewItem.IsSelected) return;

      foreach (var selectedRecord in FileRecordList.SelectedItems.Cast<FileRecord>())
      {
        if (selectedRecord != record)
        {
          selectedRecord.PackingStrategy = (PackingStrategy)comboBox.SelectedItem;
        }
      }
    }

    private void FileRecordList_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Delete)
      {
        var selectedRecords = FileRecordList.SelectedItems.Cast<FileRecord>().ToList();
        _fileRecordManager.RemoveFileRecords(selectedRecords);
        FileRecordList.SelectedItems.Clear();
      }
    }

    private string GetPackageName()
    {
      var gameDirName = new DirectoryInfo(_viewModel.OriginalFilesPath).Name;
      if (string.IsNullOrEmpty(gameDirName))
        return Properties.Resources.DefaultPackageName;

      var words = gameDirName.Split(' ', '_');
      var capitalizedWords = words.Where(word => word.Length > 0).Select(word => char.ToUpper(word[0]) + word.Substring(1).ToLower());
      var gameName = string.Join("_", capitalizedWords);

      return $"Ukrainify.{gameName}";
    }
  }
}
