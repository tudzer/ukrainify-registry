﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace PackageCreator
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private void GameTitle_TextChanged(object sender, TextChangedEventArgs e)
    {
      var escapedName = Regex.Replace(GameTitle.Text, "[^a-zA-Z0-9 ]", string.Empty);
      GameIdentifiers.Text = GameTitle.Text;
      AssemblyName.Text = escapedName;
      ProjectName.Text = ToProjectName(escapedName);
    }

    public static string ToProjectName(string input)
    {
      if (string.IsNullOrEmpty(input))
        return input;

      var words = input.Split(new char[] { ' ', '_' });
      var capitalizedWords = words.Where(word => word.Length > 0).Select(word => Char.ToUpper(word[0]) + word.Substring(1).ToLower());
      return String.Join("_", capitalizedWords);
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      Creator.Create(ProjectName.Text, AssemblyName.Text, GameTitle.Text, GameIdentifiers.Text, Hero.Value, Logo.Value);
    }
  }
}
