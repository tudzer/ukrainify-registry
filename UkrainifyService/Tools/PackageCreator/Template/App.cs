﻿using GameLib.Core;
using GameLib.Plugin.Steam;
using GameLib.Plugin.Gog;
using GameLib.Plugin.Epic;
using System;
using System.Collections.Generic;
using System.Windows;
using Ukrainify.Installer;

namespace Ukrainify
{
  internal class App : Application
  {
    private const string gameTitle = "{GameTitle}";
    private static readonly string[] gameIdentifiers = { {GameIdentifiers} };

    public App()
    {
      ResourceDictionary resources = new ResourceDictionary();
      resources.Source = new Uri("/Ukrainify.Installer;component/SharedResources.xaml", UriKind.RelativeOrAbsolute);
      Current.Resources.MergedDictionaries.Add(resources);
    }

    [STAThread]
    public static void Main()
    {
      var launchers = new List<ILauncher>
      {
        new SteamLauncher(),
        new GogLauncher(),
        new EpicLauncher(),
      };
      new App().Run(new InstallerWindow(gameTitle, gameIdentifiers, launchers));
    }
  }
}
