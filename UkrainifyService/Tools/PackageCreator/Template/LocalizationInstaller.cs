﻿using System.IO;

namespace Ukrainify
{
  public class LocalizationInstaller : ILocalizationInstaller
  {
    private readonly GameInstallation gameInstallation;

    public LocalizationInstaller(string gameInstallationPath)
    {
      gameInstallation = new GameInstallation(gameInstallationPath);
    }

    public bool IsInstalled => false; // IsInstalled check goes here

    public bool IsInstallable => gameInstallation.IsSupported;
    
    public void Install()
    {
      if (IsInstalled) return;

      // Installation code goes here
    }

    public void Uninstall()
    {
      if (!IsInstalled) return;

      // Uninstallation code goes here
    }
  }
}
