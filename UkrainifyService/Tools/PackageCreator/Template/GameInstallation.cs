﻿using System.IO;
using Ukrainify.Localization;

namespace Ukrainify
{
  internal class GameInstallation : GameInstallationBase
  {
    private const string ExecutableRelativePath = null;

    public string BasePath { get; }

    public GameInstallation(string path): base(path, ExecutableRelativePath)
    {
    
    }

    public override bool IsSupported => Directory.Exists(BasePath);
  }
}
