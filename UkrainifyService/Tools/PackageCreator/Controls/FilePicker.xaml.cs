﻿using System;
using System.Windows;
using System.Windows.Controls;


namespace PackageCreator.Controls
{
  /// <summary>
  /// Interaction logic for FilePicker.xaml
  /// </summary>
  public partial class FilePicker : UserControl
  {
    public string Caption
    {
      get => (string)GetValue(CaptionProperty);
      set => SetValue(CaptionProperty, value);
    }

    public string Value => Input.Text;

    public FilePicker()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty CaptionProperty =
      DependencyProperty.Register(
        "Caption",
        typeof(string),
        typeof(FilePicker)
      );

    private void Input_PreviewDragOver(object sender, DragEventArgs e)
    {
      e.Effects = GetSinglePngFile(e) != null ? DragDropEffects.Copy : DragDropEffects.None;
      e.Handled = true;
    }

    private void Input_PreviewDrop(object sender, DragEventArgs e)
    {
      Input.Text = GetSinglePngFile(e);
    }

    private string GetSinglePngFile(DragEventArgs e)
    {
      if (e.Data.GetDataPresent(DataFormats.FileDrop))
      {
        var files = e.Data.GetData(DataFormats.FileDrop) as string[];
        if (files != null && files.Length == 1)
        {
          var file = files[0];
          if (file.EndsWith(".png", StringComparison.OrdinalIgnoreCase))
          {
            return file;
          }
        }
      }

      return null;
    }
  }
}
