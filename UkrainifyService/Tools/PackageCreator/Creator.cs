﻿using System;
using System.IO;
using System.Linq;

namespace PackageCreator
{
  internal class Creator
  {
    public static void Create(string projectName, string assemblyName, string gameTitle, string gameIdentifiers, string heroPath, string logoPath)
    {
      var sourcePath = "Template";
      var targetPath = $"../../../../Installers/Ukrainify.{projectName}";

      // Create directories
      foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
      {
        Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
      }
      Directory.CreateDirectory(Path.Combine(targetPath, "Resources"));

      // Create files
      foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
      {
        File.Copy(newPath, newPath.Replace(sourcePath, targetPath).Replace("{ProjectName}", projectName), true);
      }

      // Parse game identifiers
      string identifiers = string.Join(", ", gameIdentifiers.Split(';').Select(identifier => $"\"{identifier}\""));

      // Fill variables in files
      ReplaceInFile(Path.Combine(targetPath, $"Ukrainify.{projectName}.csproj"), "{AssemblyName}", assemblyName);
      ReplaceInFile(Path.Combine(targetPath, "Properties/AssemblyInfo.cs"), "{GameTitle}", gameTitle);
      ReplaceInFile(Path.Combine(targetPath, "App.cs"), "{GameTitle}", gameTitle);
      ReplaceInFile(Path.Combine(targetPath, "App.cs"), "{GameIdentifiers}", identifiers);
      ReplaceInFile(Path.Combine(targetPath, "Properties/AssemblyInfo.cs"), "{Year}", DateTime.Now.Year.ToString());

      // Add images
      var brandingFolder = Path.Combine(targetPath, "Branding");
      Directory.CreateDirectory(brandingFolder);
      File.Copy(heroPath, Path.Combine(brandingFolder, "hero.png"));
      File.Copy(logoPath, Path.Combine(brandingFolder, "logo.png"));

      // Move
    }

    private static void ReplaceInFile(string filePath, string key, string replacement)
    {
      var text = File.ReadAllText(filePath);
      File.WriteAllText(filePath, text.Replace(key, replacement));
    }
  }
}
