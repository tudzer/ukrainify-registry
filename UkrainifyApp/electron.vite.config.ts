import { defineConfig, externalizeDepsPlugin } from 'electron-vite'
import path from 'path'

const resolveConfig = {
  alias: {
    '~': path.resolve(__dirname, './src/preload'),
    '@': path.resolve(__dirname, './resources')
  }
}

export default defineConfig({
  main: {
    plugins: [externalizeDepsPlugin()],
    resolve: resolveConfig
  },
  preload: {
    resolve: resolveConfig,
    build: {
      rollupOptions: {
        output: {
          interop: 'compat'
        }
      }
    },
    plugins: [externalizeDepsPlugin()]
  },
  renderer: {
    resolve: resolveConfig
  }
})
