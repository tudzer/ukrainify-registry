import { installer } from '~/installer'
import { ActionType } from './ActionType'
import { DialogState } from './DialogState'
import { store } from './DialogStore'

interface ActionParams {
  gameId: string
  localizationId: string
}

async function handleCheckInstalledState({ gameId, localizationId }: ActionParams): Promise<void> {
  const { selectedPlatform } = store
  if (!selectedPlatform) return

  const isInstalled = await installer.isInstalled(gameId, localizationId, selectedPlatform)
  store.setState(isInstalled ? DialogState.AlreadyInstalled : DialogState.NotInstalled)
}

async function handleSpecifyGamePath({ gameId, localizationId }: ActionParams): Promise<void> {
  const result = await installer.specifyPath(gameId, localizationId)
  if (result === null) return
  if (result.isSupported) {
    store.setCustomGamePath(result.path)
    store.setState(result.isInstalled ? DialogState.AlreadyInstalled : DialogState.MaybeValidPath)
  } else {
    store.setState(DialogState.InvalidPath)
  }
}

async function handleInstallLocalization({ gameId, localizationId }: ActionParams): Promise<void> {
  const { selectedPlatform, customGamePath } = store
  if (!selectedPlatform) return

  const installed = await installer.install(
    gameId,
    localizationId,
    selectedPlatform,
    customGamePath
  )
  store.setState(installed ? DialogState.JustInstalled : DialogState.InstallFailed)
}

async function handleUninstallLocalization({
  gameId,
  localizationId
}: ActionParams): Promise<void> {
  const { selectedPlatform, customGamePath } = store
  if (!selectedPlatform) return

  const installed = await installer.uninstall(
    gameId,
    localizationId,
    selectedPlatform,
    customGamePath
  )
  store.setState(installed ? DialogState.JustUninstalled : DialogState.UninstallFailed)
}

async function handleLaunchGame({ gameId }: ActionParams): Promise<void> {
  const { selectedPlatform, customGamePath } = store
  if (!selectedPlatform) return

  await installer.launchGame(gameId, selectedPlatform, customGamePath)
  store.setState(DialogState.GameLaunched)
}

const actionHandlers = {
  [ActionType.CheckInstalledState]: handleCheckInstalledState,
  [ActionType.SpecifyPath]: handleSpecifyGamePath,
  [ActionType.InstallLocalization]: handleInstallLocalization,
  [ActionType.UninstallLocalization]: handleUninstallLocalization,
  [ActionType.LaunchGame]: handleLaunchGame
}

export async function handleAction(action: ActionType, params: ActionParams): Promise<void> {
  try {
    store.setLoading(true)
    await actionHandlers[action](params)
  } catch {
    store.setState(DialogState.Error)
  } finally {
    store.setLoading(false)
  }
}
