export enum ActionType {
  CheckInstalledState,
  SpecifyPath,
  InstallLocalization,
  UninstallLocalization,
  LaunchGame
}
