export enum DialogState {
  Initial,
  SpecifyGamePath,
  MaybeValidPath,
  InvalidPath,
  NotInstalled,
  AlreadyInstalled,
  JustInstalled,
  JustUninstalled,
  GameLaunched,
  InstallFailed,
  UninstallFailed,
  Error
}
