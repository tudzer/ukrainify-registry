// import React, { useEffect } from 'react'
// import styled from 'styled-components'
// import { observer } from 'mobx-react'

// import folderIcon from '@/folder.png'
// import { PlatformType } from '~/domain/PlatformType'
// import { Localization } from '~/domain/Localization'
// import { ActionType } from './ActionType'
// import { Button } from './components/Button'
// import { DialogState } from './DialogState'
// import { FadeInOut } from './components/FadeInOut'
// import { handleAction } from './actionHandler'
// import { PlatformIcon } from './components/PlatformIcon'
// import { PlatformSelector } from './components/PlatformSelector'
// import { Spinner } from './components/Spinner'
// import { store } from './DialogStore'

// const platformIcons: Record<PlatformType, string> = {
//   [PlatformType.Steam]: '/images/thumbs/001/0012155_icons8-steam-48.png',
//   [PlatformType.Gog]: '/images/thumbs/000/0004169_gog.png',
//   [PlatformType.Epic]: '/images/thumbs/000/0004491_epic.png',
//   [PlatformType.Custom]: folderIcon
// }

// interface DialogProps {
//   gameId: string
//   localization: Localization
// }

// const Content = styled.div`
//   .fade-enter {
//     opacity: 0;
//     visibility: hidden;
//   }

//   .fade-enter-active {
//     opacity: 1;
//     visibility: visible;
//     transition:
//       opacity 300ms,
//       visibility 300ms;
//   }

//   .fade-exit {
//     opacity: 1;
//     visibility: visible;
//   }

//   .fade-exit-active {
//     opacity: 0;
//     visibility: hidden;
//     transition:
//       opacity 300ms,
//       visibility 300ms;
//   }

//   display: flex;
//   flex-direction: column;
//   align-items: center;
// `

// const Description = styled.div`
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   text-align: center;
//   color: #777;
//   font-size: 14px;
//   font-family: e-Ukraine;
//   height: 40px;
//   transition: opacity 300ms ease-in-out;
// `

// const ButtonContainer = styled.div`
//   height: 16px;
//   transition: opacity 300ms ease-in-out;
// `

// const component: React.FC<DialogProps> = ({ gameId, localization }) => {
//   const { localizationId } = localization
//   const { loading, selectedPlatform, state } = store
//   const { action, description } = state

//   useEffect(() => {
//     async function handlePlatformSelection(): Promise<void> {
//       if (!selectedPlatform) return
//       if (selectedPlatform === PlatformType.Custom) {
//         store.setState(DialogState.SpecifyGamePath)
//       } else {
//         await handleAction(ActionType.CheckInstalledState, { gameId, localizationId })
//       }
//     }

//     void handlePlatformSelection()
//   }, [selectedPlatform])

//   const platforms: PlatformType[] = Object.values(PlatformType).filter(
//     (platform) => platform == PlatformType.Custom || localization.platforms.includes(platform)
//   )

//   const handlePlatformChange = (platform: PlatformType): void => {
//     store.setSelectedPlatform(platform)
//     if (platform !== PlatformType.Custom) {
//       store.setCustomGamePath(null)
//     }
//   }

//   return (
//     <Content>
//       <PlatformSelector>
//         {platforms.map((platform) => (
//           <PlatformIcon
//             key={platform}
//             src={platformIcons[platform]}
//             active={platform === selectedPlatform}
//             onClick={() => handlePlatformChange(platform)}
//           />
//         ))}
//       </PlatformSelector>

//       {loading ? (
//         <Spinner />
//       ) : (
//         <React.Fragment>
//           <Description>
//             <FadeInOut>{description}</FadeInOut>
//           </Description>
//           <ButtonContainer>
//             {action && (
//               <Button onClick={() => void handleAction(action.type, { gameId, localizationId })}>
//                 <FadeInOut>{action.label}</FadeInOut>
//               </Button>
//             )}
//           </ButtonContainer>
//         </React.Fragment>
//       )}
//     </Content>
//   )
// }

// export const DialogContent = observer(component)
