// import styled, { css } from 'styled-components'

// interface PlatformIconProps {
//   active: boolean
// }

// // export const PlatformIcon: React.FC<PlatformIconProps> = (props) => <img src={props.iconUrl}></img>

// export const PlatformIcon = styled.img<PlatformIconProps>`
//   width: 32px;
//   cursor: pointer;
//   aspect-ratio: 1;
//   transition: all 0.1s ease-in-out;

//   &:hover {
//     transform: scale(1.3);
//   }

//   ${(props) =>
//     props.active &&
//     css`
//       transform: scale(1.5);
//       &:hover {
//         transform: scale(1.5);
//       }
//     `}
// `
