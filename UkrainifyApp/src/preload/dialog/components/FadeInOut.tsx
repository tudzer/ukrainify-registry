// import React, { useState, useEffect } from 'react'
// import styled from 'styled-components'

// const Container = styled.div`
//   transition: opacity 0.5s ease;
//   opacity: 1;
//   white-space: pre-wrap;

//   &.fade-out {
//     opacity: 0;
//   }
// `

// interface FadeInOutProps {
//   duration?: number
//   children: string | JSX.Element | JSX.Element[] | (() => JSX.Element)
// }

// export const FadeInOut: React.FC<FadeInOutProps> = ({ children, duration = 300 }) => {
//   const [content, setContent] = useState(children)
//   const [fade, setFade] = useState(true)

//   useEffect(() => {
//     setFade(false)

//     const timeoutId = setTimeout(() => {
//       setContent(children)
//       setFade(true)
//     }, duration)

//     return () => clearTimeout(timeoutId)
//   }, [children])

//   return <Container className={fade ? '' : 'fade-out'}>{content}</Container>
// }
