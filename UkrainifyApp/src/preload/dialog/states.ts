import strings from '@/strings.json'
import { ActionType } from './ActionType'
import { DialogState } from './DialogState'

export interface StateDefinition {
  description: string
  action?: {
    type: ActionType
    label: string
  }
}

export const states: Record<DialogState, StateDefinition> = {
  [DialogState.Initial]: {
    description: strings.selectPlatformOrSpecifyPath
  },
  [DialogState.SpecifyGamePath]: {
    description: strings.specifyInstallationPath,
    action: {
      type: ActionType.SpecifyPath,
      label: strings.specifyPath
    }
  },
  [DialogState.MaybeValidPath]: {
    description: strings.maybeValidPath,
    action: {
      type: ActionType.InstallLocalization,
      label: strings.installLocalization
    }
  },
  [DialogState.InvalidPath]: {
    description: strings.invalidPath,
    action: {
      type: ActionType.SpecifyPath,
      label: strings.specifyOtherPath
    }
  },
  [DialogState.NotInstalled]: {
    description: strings.notYetInstalled,
    action: {
      type: ActionType.InstallLocalization,
      label: strings.installLocalization
    }
  },
  [DialogState.AlreadyInstalled]: {
    description: strings.alreadyInstalled,
    action: {
      type: ActionType.UninstallLocalization,
      label: strings.uninstallLocalization
    }
  },
  [DialogState.JustInstalled]: {
    description: strings.localizationInstalled,
    action: {
      type: ActionType.LaunchGame,
      label: strings.launchGame
    }
  },
  [DialogState.JustUninstalled]: {
    description: strings.localizationUninstalled,
    action: {
      type: ActionType.LaunchGame,
      label: strings.launchGame
    }
  },
  [DialogState.GameLaunched]: {
    description: strings.gameLaunched
  },
  [DialogState.InstallFailed]: {
    description: strings.installFailed
  },
  [DialogState.UninstallFailed]: {
    description: strings.uninstallFailed
  },
  [DialogState.Error]: {
    description: strings.errorOcurred
  }
}
