// import React from 'react'
// import { createRoot } from 'react-dom/client'
// import strings from '@/strings.json'
// import { Localization } from '~/domain/Localization'
// import { DialogContent } from './DialogContent'
// import { store } from './DialogStore'

// declare function displayInfoPopup(
//   title: string,
//   content: string,
//   url?: string,
//   action?: string,
//   width?: number
// ): void

// export function addDialogLink(
//   container: Element,
//   gameId: string,
//   localization: Localization
// ): void {
//   const leftActions = container.querySelector('.footer__actions-left-side')
//   if (leftActions) {
//     const link = document.createElement('a')
//     link.addEventListener('click', () => openDialog(gameId, localization))
//     link.classList.add('footer__action')
//     link.classList.add('footer__action--download')
//     link.innerText = strings.install
//     leftActions.replaceChildren(link)
//   }
// }

// const openDialog = (gameId: string, localization: Localization): void => {
//   const dialogContainerId = 'install-dialog-content'
//   displayInfoPopup(
//     strings.localizationInstallation,
//     `<div id="${dialogContainerId}"></div>`,
//     undefined,
//     undefined,
//     400
//   )

//   const container = document.getElementById(dialogContainerId)
//   if (container) {
//     const root = createRoot(container)
//     store.reset()
//     root.render(<DialogContent gameId={gameId} localization={localization} />)
//   }
// }
