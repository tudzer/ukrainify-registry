export enum Command {
  GetLocalizations = 'GetLocalizations',
  IsLocalizationInstalled = 'IsLocalizationInstalled',
  SpecifyCustomGamePath = 'SpecifyCustomGamePath',
  InstallLocalization = 'InstallLocalization',
  UninstallLocalization = 'UninstallLocalization',
  LaunchGame = 'LaunchGame'
}
