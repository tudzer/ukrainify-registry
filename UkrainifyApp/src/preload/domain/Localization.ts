import { PlatformType } from './PlatformType'

export interface Localization {
  localizationId: string
  platforms: PlatformType[]
}
