export enum PlatformType {
  Steam = 'steam',
  Gog = 'gog',
  Epic = 'epic',
  Custom = 'custom'
}
