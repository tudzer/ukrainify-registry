import { ChildProcessWithoutNullStreams, spawn } from 'child_process'

export interface IpcCommand {
  command: string
  args: string[]
}

interface IpcCommandResult {
  id: number
  success: boolean
  result: unknown
}

export class InstallerProcess {
  private nextCommandId = 0
  private runningCommands = new Map<number, Callback<unknown>>()

  constructor(private process: ChildProcessWithoutNullStreams) {
    process.stdout.on('data', (data: string) => {
      console.debug(`Received result from installer:\n${data}`)
      const { id, success, result } = JSON.parse(data) as IpcCommandResult
      const resolve = this.runningCommands.get(id)
      if (!resolve) {
        throw Error('Unexpected command result')
      }
      resolve({ result, success })
      this.runningCommands.delete(id)
    })
  }

  public sendCommand(command: string, args: object): Promise<unknown> {
    const id = this.nextCommandId++
    const commandDefinition = { id, command, args }
    const data = JSON.stringify(commandDefinition)
    this.process.stdin.write(data + '\n')
    console.debug(`Sent command to installer:\n${data}`)

    return new Promise((resolve) => {
      this.runningCommands.set(id, resolve)
    })
  }

  public static launch(): InstallerProcess {
    const process = spawn(import.meta.env.MAIN_VITE_SERVICE_EXECUTABLE_PATH)
    return new InstallerProcess(process)
  }
}
