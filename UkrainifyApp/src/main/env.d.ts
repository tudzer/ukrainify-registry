/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly MAIN_VITE_SERVICE_EXECUTABLE_PATH: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
