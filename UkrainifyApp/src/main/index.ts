import { handleSquirrelCommand } from './squirrel'
import { run } from './main'

const squirrelCommand = handleSquirrelCommand()
if (squirrelCommand === null) {
  run()
}
