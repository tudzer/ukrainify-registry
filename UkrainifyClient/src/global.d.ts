// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare type Any = any
declare type Callback<TArgs = unknown, TReturnType = void> = (args: TArgs) => TReturnType
declare type Maybe<T> = T | undefined
declare type Nullable<T> = T | null
