import { SquirrelCommand, handleSquirrelCommand } from './squirrel'
import { run } from './main'
import { installProtocol } from './protocol'

void installProtocol(
  'ukrainify',
  '%LocalAppData%/Ukrainify/Ukrainify.exe',
  '%LocalAppData%/Ukrainify/app.ico'
)

const squirrelCommand = handleSquirrelCommand()
if (squirrelCommand === null) {
  run()
} else {
  if (squirrelCommand === SquirrelCommand.Install) {
    void installProtocol(
      'ukrainify',
      '%LocalAppData%/Ukrainify/Ukrainify.exe',
      '%LocalAppData%/Ukrainify/app.ico'
    )
  }
}
