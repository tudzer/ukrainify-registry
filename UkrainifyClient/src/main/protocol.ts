import regedit from 'regedit'

export async function installProtocol(
  protocol: string,
  appPath: string,
  iconPath: string
): Promise<void> {
  const rootKey = `HKCU\\Software\\Classes\\${protocol}`
  const iconKey = `${rootKey}\\DefaultIcon`
  const commandKey = `${rootKey}\\shell\\open\\command`

  const command = `"${appPath}" "%1"`
  const icon = `"${iconPath}"`

  const values: regedit.RegistryItemPutCollection = {
    [rootKey]: {
      '@': {
        value: `URL:${protocol}`,
        type: 'REG_DEFAULT'
      },
      'URL Protocol': {
        value: '',
        type: 'REG_SZ'
      }
    },
    [iconKey]: {
      '@': {
        value: icon,
        type: 'REG_DEFAULT'
      }
    },
    [commandKey]: {
      '@': {
        value: command,
        type: 'REG_DEFAULT'
      }
    }
  }

  try {
    await regedit.promisified.createKey([rootKey, iconKey, commandKey])
    await regedit.promisified.putValue(values)
    console.log(`Protocol ${protocol} installed successfully.`)
  } catch (err) {
    console.error('Failed to install protocol:', err)
  }
}

// Usage example
