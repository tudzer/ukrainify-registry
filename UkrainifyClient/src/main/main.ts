import { app, shell, BrowserWindow, ipcMain } from 'electron'
import { join } from 'path'
import { electronApp, optimizer } from '@electron-toolkit/utils'
import strings from '@/strings.json'
import icon from '@/icon.png?asset'
import customCss from '~/styles.css?raw'
import { InstallerProcess, IpcCommand } from './installer-ipc'

export function run(): void {
  const appUrl = 'https://kuli.com.ua/stick-fight-the-game'
  const preloadScript = join(__dirname, '../preload/index.js')

  process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = 'true'
  app.commandLine.appendSwitch('disable-features', 'WidgetLayering')

  function createWindow(): void {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
      minWidth: 900,
      minHeight: 600,
      show: false,
      autoHideMenuBar: true,
      ...(process.platform === 'linux' ? { icon } : {}),
      webPreferences: {
        contextIsolation: false,
        preload: preloadScript,
        sandbox: false
      }
    })

    mainWindow.on('ready-to-show', () => {
      mainWindow.show()
    })

    mainWindow.on('page-title-updated', (e) => {
      e.preventDefault()
    })

    mainWindow.webContents.on('did-finish-load', () => {
      mainWindow.webContents.insertCSS(customCss)
    })

    mainWindow.webContents.on('will-navigate', (e, url) => {
      const appUrl = new URL(mainWindow.webContents.getURL())
      if (!url.startsWith(appUrl.origin)) {
        shell.openExternal(url)
        e.preventDefault()
      }
    })

    mainWindow.webContents.setWindowOpenHandler((details) => {
      shell.openExternal(details.url)
      return { action: 'deny' }
    })

    mainWindow.setTitle(strings.appName)
    mainWindow.loadURL(appUrl)
  }

  app.whenReady().then(() => {
    const installerProcess = InstallerProcess.launch()
    ipcMain.handle('command', async (_event, { command, args }: IpcCommand) => {
      return installerProcess.sendCommand(command, args)
    })

    // Set app user model id for windows
    electronApp.setAppUserModelId('com.electron')

    // Default open or close DevTools by F12 in development
    // and ignore CommandOrControl + R in production.
    // see https://github.com/alex8088/electron-toolkit/tree/master/packages/utils
    app.on('browser-window-created', (_, window) => {
      optimizer.watchWindowShortcuts(window)
    })

    createWindow()

    app.on('activate', function () {
      // On macOS it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
  })

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })
}
