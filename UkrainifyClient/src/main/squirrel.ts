import path from 'path'
import { spawn } from 'child_process'
import { app } from 'electron'

export enum SquirrelCommand {
  Install,
  Uninstall,
  Update,
  Obsolete
}

const commands = {
  '--squirrel-install': SquirrelCommand.Install,
  '--squirrel-updated': SquirrelCommand.Update,
  '--squirrel-uninstall': SquirrelCommand.Uninstall,
  '--squirrel-obsolete': SquirrelCommand.Obsolete
}

function run(args: string[], done: Callback): void {
  const updateExe = path.resolve(path.dirname(process.execPath), '..', 'Update.exe')

  spawn(updateExe, args, {
    detached: true
  }).on('close', done)
}

export function handleSquirrelCommand(): SquirrelCommand | null {
  if (process.platform !== 'win32') return null

  const cmd = commands[process.argv[1]] ?? null
  const target = path.basename(process.execPath)

  switch (cmd) {
    case SquirrelCommand.Install:
    case SquirrelCommand.Update:
      run(['--createShortcut=' + target + ''], app.quit)
      break
    case SquirrelCommand.Uninstall:
      run(['--removeShortcut=' + target + ''], app.quit)
      break
    case SquirrelCommand.Obsolete:
      app.quit()
  }
  return cmd
}
