export interface CustomInstallationState {
  isInstalled: boolean
  isSupported: boolean
  path: string
}
