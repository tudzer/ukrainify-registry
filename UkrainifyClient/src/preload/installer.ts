import { ipcRenderer } from 'electron'
import { Localization } from './domain/Localization'
import { PlatformType } from './domain/PlatformType'
import { CustomInstallationState } from './domain/CustomInstallationState'
import { Command } from './Command'

async function sendCommand<T>(command: string, args: object): Promise<T> {
  const { success, result } = await ipcRenderer.invoke('command', { command, args })
  if (!success) {
    throw new Error('Command failed to execute')
  }
  return result
}

export const installer = {
  getLocalizations(gameId: string): Promise<Localization[]> {
    return sendCommand<Localization[]>(Command.GetLocalizations, { gameId })
  },

  isInstalled(gameId: string, localizationId: string, platform: PlatformType): Promise<boolean> {
    return sendCommand(Command.IsLocalizationInstalled, { gameId, localizationId, platform })
  },

  specifyPath(gameId: string, localizationId: string): Promise<Nullable<CustomInstallationState>> {
    return sendCommand(Command.SpecifyCustomGamePath, { gameId, localizationId })
  },

  install(
    gameId: string,
    localizationId: string,
    platform: PlatformType,
    path: Nullable<string>
  ): Promise<boolean> {
    return sendCommand(Command.InstallLocalization, { gameId, localizationId, platform, path })
  },

  uninstall(
    gameId: string,
    localizationId: string,
    platform: PlatformType,
    path: Nullable<string>
  ): Promise<boolean> {
    return sendCommand(Command.UninstallLocalization, { gameId, localizationId, platform, path })
  },

  launchGame(gameId: string, platform: PlatformType, path: Nullable<string>): Promise<void> {
    return sendCommand(Command.LaunchGame, { gameId, platform, path })
  }
}
