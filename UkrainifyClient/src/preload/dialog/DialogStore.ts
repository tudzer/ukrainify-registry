import { makeAutoObservable } from 'mobx'
import { PlatformType } from '~/domain/PlatformType'
import { DialogState } from './DialogState'
import { StateDefinition, states } from './states'

class DialogStore {
  loading = false
  selectedPlatform: Nullable<PlatformType> = null
  customGamePath: Nullable<string> = null
  state: StateDefinition = states[DialogState.Initial]

  constructor() {
    makeAutoObservable(this)
  }

  reset(): void {
    this.loading = false
    this.selectedPlatform = null
    this.customGamePath = null
    this.state = states[DialogState.Initial]
  }

  setCustomGamePath(path: Nullable<string>): void {
    this.customGamePath = path
  }

  setLoading(loading: boolean): void {
    this.loading = loading
  }

  setSelectedPlatform(platform: PlatformType): void {
    this.selectedPlatform = platform
    if (platform !== PlatformType.Custom) {
      this.customGamePath = null
    }
  }

  setState(newState: DialogState): void {
    this.state = states[newState]
  }
}

export const store = new DialogStore()
