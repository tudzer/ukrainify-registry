import styled from 'styled-components'

export const Button = styled.a`
  color: #2e53ac !important;
  font-family: e-Ukraine;
  font-weight: bold;
  font-size: 13px;
`
