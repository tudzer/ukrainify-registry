import { installer } from './installer'
import { addDialogLink } from './dialog'

const gamePageSelector = '.prodect-title-name'
const localizationContainerSelector = '.footer__actions'

function getCurrentGameId(): Nullable<string> {
  return document.querySelector(gamePageSelector) ? location.pathname.slice(1) : null
}

function getLocalizationId(container: Element): Maybe<string> {
  const link = container.querySelector('.footer__action--download') as HTMLAnchorElement
  return link?.href.split('/').pop()
}

async function setup(): Promise<void> {
  document.addEventListener('DOMContentLoaded', async () => {
    const gameId = getCurrentGameId()
    if (!gameId) return

    const localizations = await installer.getLocalizations(gameId)
    const localizationContainers = [...document.querySelectorAll(localizationContainerSelector)]
    for (const container of localizationContainers) {
      const id = getLocalizationId(container)
      if (!id) continue
      const localization = localizations.find((l) => l.localizationId == id)
      if (localization) {
        addDialogLink(container, gameId, localization)
      }
    }
  })
}

void setup()
