const { loadEnv } = require('vite')

const env = loadEnv('production', process.cwd())

module.exports = {
  packagerConfig: {
    appCopyright: 'Copyright © 2023 Tudzer. All rights reserved.',
    extraResource: [env.VITE_SERVICE_EXECUTABLE_PATH],
    icon: './build/icon.ico',
    ignore: [
      /^\/src/,
      /(.eslintrc.json)|(.gitignore)|(electron.vite.config.ts)|(forge.config.cjs)|(tsconfig.*)/
    ]
  },
  rebuildConfig: {},
  makers: [
    {
      name: '@electron-forge/maker-squirrel',
      config: {
        name: 'Ukrainify',
        exeIcon: './build/icon.ico',
        setupIcon: './build/icon.ico',
        iconUrl:
          'https://gitlab.com/tudzer/ukrainify/-/raw/fb27ee544fea3961b42e4e2ed8edb1feab1288ec/icon.ico',
        loadingGif: './build/background.gif',
        setupExe: 'UkrainifySetup.exe',
        skipUpdateIcon: true,
        authors: 'Tudzer',
        description: 'Bringing Ukrainian language support to your favorite games with ease.'
      }
    }
  ]
}
